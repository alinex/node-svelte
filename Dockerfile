# https://gist.github.com/aradalvand/04b2cad14b00e5ffe8ec96a3afbb34fb

FROM node:20-alpine AS builder
WORKDIR /app
COPY package*.json ./
RUN npm ci
COPY . ./
RUN npm run build
RUN npm prune --production

FROM node:20-alpine
WORKDIR /app
COPY --from=builder /app/build ./
USER node:node
EXPOSE 3000
ENV NODE_ENV=production
CMD [ "node", "." ]

#FROM node:20-alpine
#
## install dependencies
#WORKDIR /app
#COPY build .
#RUN npm install --omit=dev
#USER node:node
#ENV NODE_ENV=production
#EXPOSE 3000
#CMD ["node", "./index.js"]
