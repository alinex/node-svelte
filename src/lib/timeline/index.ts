import Timeline from '$lib/timeline/Timeline.svelte';
import BusinessItem from '$lib/timeline/BusinessItem.svelte';
import BusinessDetail from '$lib/timeline/BusinessDetail.svelte';
import PrivateDetail from '$lib/timeline/PrivateDetail.svelte';
import EducationItem from '$lib/timeline/EducationItem.svelte';
import BirthItem from '$lib/timeline/BirthItem.svelte';

export { Timeline, BusinessItem, BusinessDetail, PrivateDetail, EducationItem, BirthItem }
