export default {
    timeline: "space-y-8 relative before:absolute before:inset-0 before:ml-6 before:-translate-x-px lg:before:ml-[22.4rem] lg:before:translate-x-0 xs:before:h-full xs:before:w-1 xs:before:bg-gradient-to-b xs:before:from-transparent xs:before:via-slate-100 xs:before:to-transparent",
    entry: 'lg:flex items-center lg:space-x-6 mb-3',
    sub: 'lg:flex items-center lg:space-x-6',
    left: 'flex items-center 2xs:space-x-4 lg:space-x-reverse md:text-right',
    primary:
        'hidden 2xs:flex items-center justify-center w-12 h-12 rounded-full bg-primary-500 text-black shadow lg:order-1',
    primaryXS:
        'hidden xs:flex items-center justify-center w-6 h-6 m-3 rounded-full bg-primary-500 text-black shadow lg:order-1',
    secondaryXS:
        'hidden xs:flex items-center justify-center w-6 h-6 m-3 rounded-full bg-secondary-500 text-black shadow lg:order-1',
    date: 'font-caveat font-medium text-xl text-primary-500 lg:w-80',
    heading: 'text-slate-200 mt-4 xs:mt-0 xs:ml-16',
    headingP: 'text-slate-400 mt-4 xs:mt-0 xs:ml-16',
    headingDetail: 'text-slate-200 mt-4 xs:-mt-9 lg:mt-0 xs:ml-16',
    headingDetailP: 'text-slate-400 mt-4 xs:-mt-9 lg:mt-0 xs:ml-16',
    title: 'text-slate-00 font-bold',
    detail: 'px-4 text-slate-200 shadow xs:ml-16 lg:ml-[26.4rem]',
    detailP: 'px-4 text-slate-400 shadow xs:ml-16 lg:ml-[26.4rem]'
};
