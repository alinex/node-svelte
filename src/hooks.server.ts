import { sequence } from '@sveltejs/kit/hooks';
import { detectLocale } from '$i18n/i18n-util'
import { loadAllLocales } from '$i18n/i18n-util.sync'
import type { Handle } from '@sveltejs/kit';
import { initAcceptLanguageHeaderDetector } from 'typesafe-i18n/detectors'

loadAllLocales()

const setLanguage: Handle = async ({ event, resolve }) => {
    const locale = detectLocale(initAcceptLanguageHeaderDetector(event.request))
    // replace html lang attribute with correct language
    return resolve(event, { transformPageChunk: ({ html }) => html.replace('%lang%', locale) })
}

export const handle = sequence(setLanguage);
