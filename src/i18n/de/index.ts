import type { Translation } from '../i18n-types'

const de = {
	layout: {
		buttons: {
			vita: 'Vita',
			vitaTitle: 'Lebenslauf, Fähigkeiten, Erfahrungen',
			works: 'Arbeiten',
			worksTitle: 'Projekte und Arbeiten',
			localeTitle: 'Sprache wählen'
		},
		logoTitle: "Startseite"
	},
	intro: {
		title: 'Was ist Alinex?',
		description: 'Entwickler Site mit persönlichen Informationen und Projekte von Alexander Schilling',
		paragraph: [
			`Dies ist ein zusammengesetzter Kunstname aus meinem Kurznamen <b>ALEX</b> und dem Begriff <b>IN</b>-ternet.`,
			`Zunächst nutzte ich dies als Arbeitstitel für alle meine Entwicklungen. Dabei sehe ich das programmieren in meinen openSource Projekten hauptsächlich als Lernprojekt, aus dem ab und zu wirklich nützliches hervorgeht. Inzwischen hat sich dies zu einem guten Baukasten als Basis für verschiedene Projekte entwickelt, die als Basis für individuelle, produktive Produkte genutzt werden.`,
			`Ich nutze diesen Begriff auch als Pseudonym und Anmeldenamen in verschiedenen digitalen Systemen. Deshalb ist diese Site auch unter dem Namen Alinex als meine private Homepage entstanden.`,
			`Lese mehr über mich und meine Arbeiten auf diesen Seiten oder meine Entwicklungen und technischen Dokumentationen die hier verlinkt sind.`,
		]
	},
	person: {
		menu: 'Person',
		title: 'Über mich',
		description: 'Einige Informationen zu Alexander Schilling',
	},
	vita: {
		menu: 'Lebenslauf',
		title: 'Lebenslauf und Erfahrungen',
		schoolTitle: 'Ausbildung',
		description: 'Zeitlicher Lebenslauf und IT Erfahrungen',
		timelineBusinessDetail: 'Berufliches Projekt',
		timelinePrivateDetail: 'Persönliches Projekt',
		divibib: {
			job: 'IT Administrator und Betrieb',
			time: 'Januar 2016 bis heute',
			company: 'divibib GmbH, D-72764 Reutlingen<br />ein Tochterunternehmen der <a href="https://www.ekz.de/">ekz Gruppe</a>',
			description: 'Betrieb und Administration der externen und internen IT Systeme mit Firewall, Loadbalancer, ESX Server mit über 200 virtuellen Server auf denen mehr als 800 produktive Anwendungen laufen mit zahlreichen Datenbanksystemen und Storages. Weiterhin Second-Level-Support, Analyse, Datenmanagement, Dokumentation, Planung, Automatisierung und Optimierung.',
			cicd: 'Entwickeln einer Pipeline, die automatisch für jedes Projekt entsprechend passende Jobs erzeugt und für einen einheitlichen Ablauf sorgt. Vom check über bauen, security, deployment, review und last test in verschiedenen Umgebungen. Und das für eine zunehmende Anzahl an Sprachen und Tools.',
			management: 'Modulares Entwicklungssystem zum Erstellen von angepassten IT Betriebstools zur System- und Daten Analyse mit automatischer Reparatur sowie zur Installation und Konfiguration auf mehreren Hosts.',
			grafana: 'Sammeln von Metriken der Systeme, Services und Daten Analyse in der Prometheus Datenbasis mit Visualisierung über angepasste Grafana Dashboards.',
			provider: 'Planung und Durchführung des Umzugs mit Optimierung der Architektur zu einem neuen Provider. Hierbei wurde besonderes Augenmerk auf Ausfallsicherheit und Performance gelegt.',
			gitlab: 'Aufbau eines privaten Servers mit Migration von Subversion. Einführung von Mattermost als privates chat system für interne und externe Teams.',
			atlassian: 'Administration, management und Einführung im Unternehmen sowie als neues Intranet.',
			nodetools: 'CoffeeScript tools als Hilfsprogramme für den Betrieb und das Server Management.',
			postgres: 'Administration, Management und Daten Analyse.'
		},
		readers: {
			job: 'IT Projekt Manager, Architekt und Betrieb',
			time: 'Oktober 2011 - Dezember 2015',
			company: '4readers GmbH, 72764 Reutlingen<br />ein Tochterunternehmen der <a href="https://www.ekz.de/en/home/">ekz Gruppe</a>',
			description: 'Planung und Realisierung einer modernen Verkaufsplattform mit digitalen Medien und Büchern. Hierzu gehörte die Entwicklung eines Shops basierend auf dem Liferay Portal, ein System zum verarbeiten von Audio/Video Daten mit Streaming durch Wasserzeichen geschützt über Wowza Server.',
			media: 'Audio/Video Medien Konvertierungs- und Verwaltungsdienste basierend auf einer Arbeitswarteschlange. Funktionell wird Formatumwandlung, Qualitätsanpassung und Wasserzeichen Vorbereitung durchgeführt.',
			facebook: 'Zu Werbezwecken habe ich Facebook Applikationen erstellt wie Adventskalender.',
			ruby: 'Ich habe ein System, geschrieben in Ruby und Python, übernommen und erweitert.'
		},
		inmedea: {
			job: 'IT Manager und Entwickler',
			time: 'Februar 2006 - September 2011',
			description:
				'Weiterführung des bisherigen Universitätsprojekt als ausgegründetes Unternehmen.'
		},
		uni: {
			job: 'Software Entwickler',
			time: 'August 2002 - Januar 2006',
			description: 'Architekt und Kernentwickler eines fallbasierten medizinischen Lernsystems.',
			java: 'Java Web System basierend auf Java Servlets und JSP auf einem Tomcat mit MySQL Datenbank.'
		},
		webhoch3: {
			job: 'Web Entwickler',
			time: 'Mai 2001 - July 2002',
			description:
				'Architekt und Entwickler eines Content-Management-Systems für mittelständische Unternehmen.'
		},
		brokat: {
			job: 'Web Entwickler',
			time: 'Oktober 1998 - Mai 2001',
			description:
				'Webserver Betrieb, Entwicklung eines Content-Management-Systems und Betreuung der Firmenwebsites im Internet und Intranet.',
			cms: 'Ein selbsterstelltes Content-Management-System auf basis von Perl und einer MySQL Datenbank, das statische Seiten erstellt.',
		},

		fh: {
			title: 'Diplom Wirtschaftsinformatiker (FH)',
			time: 'Oktober 1994 - Februar 1999',
			company: 'Fachhochschule Reutlingen',
			description: 'Studium an der Hochschule Reutlingen mit Praxissemester bei Merckle Ratiopharm, Ulm.',
			sap: 'Berater für die Einführung von SAP/R3 in mittelständischen Unternehmen.',
			sapr2: 'Bildverarbeitung mit ABAP im SAP/R2 bei Merckle Ratiopharm.',
			wago: 'Website Erstellung und inhaltliche Betreuung als Teilzeitjob.'
		},
		kolleg: {
			title: 'Fachhochschulreife',
			time: 'September 1993 - Juli 1994'
		},
		wago: {
			title: 'Kommunikationselektroniker',
			time: 'September 1991 - Juli 1993',
			description: 'Ausbildung in der Fachrichtung Funktechnik.'
		},
		berufsschule: {
			title: 'Mittlere Reife',
			time: 'September 1989 - Juli 1991',
			description: 'Inklusive erstem Lehrjahr.'
		},
		schule: {
			title: 'Hauptschulabschluß',
			time: 'September 1980 - Juli 1989',
			description: 'Ich habe meiner Lehrerin das arbeiten am Atari Computer beigebracht. Technische Abschlussprüfung al Programmierarbeit am Atari mit Relaissteuerung.'
		},
		birth: {
			title: 'Geburt',
			time: 'Januar 1974',
			description: 'Reutlingen, Deutschland'
		},

		it: {
			svelte: 'Web frontend framework für statische Sites (CSR), SSR und hybride Sites mit Fokus auf Geschwindigkeit, einfachen Aufbau und Funktion mit SvelteKit um die nächste Generation meiner Frontends zu bauen.',
			feathers: `API Programmierung mit Feathers JS, das es erlaubt mit NodeJS einfach backend services zu bauen, inklusive Authentifizierung und Autorisierung.`,
			checkup: 'Monitoring System, das es erlaubt komplexe Prüfungen einfach zu konfigurieren und nachvollziehbare Berichte und Meßwerte erstellt. Erstes echtes Produkt basierend auf meinen NodeJS Modulen: Server, Validator, DataStore...',
			rust: 'Einarbeitung in Rust mit kleinen Beispielprogrammen. Aber dann doch wieder zur Seite gelegt, um Anwendungen schnell und einfach mit JavaScript zu erzeugen.',
			quasar: 'Einarbeitung und Einsatz des auf Vue basierenden Frameworks, nachdem ich bereits Angular, React und Vue miteinander verglichen habe.',
			es6: 'Ausprobieren der neuen JavaScript Technologien von ES6 beim Bau von Modulen.',
			node: 'Ich habe NodeJS als Alternative ru Perl Scripts kennen gelernt und vor allem die asynchrone Verarbeitung als Vorteil  gesehen.',
			gadgets: 'Entwicklung von Frontend Gadgets und Effekte für Websites.',
			php: 'Einsatz von PHP mit MySQL zur Erstellung dynamischer, serverseitig erstellter Inhalte.',
			javascript: 'Eigenes Frontend Framework als Abstraktionsschicht um besser mit den Browserinkompatibilitäten umzugehen.',
			accessTitle: 'Vereinsverwaltung',
			access: 'Mit Microsoft Access Formularen und Datenbank wurde ein System für das örtliche Rote Kreuz zu Material- und Mitgliederverwaltung erstellt..',
			crossbrowser: 'Eigene crossbrowser JavaScript Bibliothek, die das einheitliche arbeiten mit IE and Netscape (IFrame vs Layer...) erlaubt.',
			delphi: 'Kleine Anwendungen habe ich mit Delphi (Pascal) erstellt.',
			web: 'Zusammen mit dem Web bin ich groß geworden mit HTML und serverseitigem Perl.',
			linux: 'Ich setzte mehr und mehr auf Linux Betriebssysteme wie Slackware, DLD, Suse, Debian...',
			windows: 'Ich wechselte von GEOS auf Windows 3.0. Hier begann ich kleine Anwendungen in Visual Basic und Turbo Pascal zu erstellen.',
			pc: 'Mein erster PC war ein 286er mit Festplatte auf dem MS-DOS und GEOS als graphischer Desktop lief.',
			amiga: 'Ich bekam einen Amiga 2000 auf dem ich Music Programmierung und mehr machte.',
			computer: 'Beginnend mit einem Kosmos Elektronik Baukasten bis zum C64 auf dem ich Basic Programmierung mit Sprites machte.'
		}
	},
	skills: {
		menu: 'Fähigkeiten',
		title: 'Wissen und Fähigkeiten',
		description: 'Mein Wissen und Fähigkeiten im IT Umfeld',
		code: {
			title: 'Sprachen',
			TypeScript: 'Viele meiner Module sind inzwischen in TypeScript, da es zusätzliche Sicherheit bringt und Fehler reduziert. Da inzwischen auch immer mehr Fremdmodule ebenfalls TypeScript unterstützen ist auch das Entwickeln damit leichter.',
			JavaScript: 'Seit es JavaScript gibt nutze ich es im Browser, und seit NodeJS als stabile Version herauskam, begann ich Perl/PHP im Server dadurch zu ersetzen. Inzwischen habe ich über 25 Jahre Erfahrung damit im Browser als funktionale, Objekt orientierte Sprache und auch die Nutzung im neuen, asynchronen Stil. Neben Web Programmen habe ich auch Programme zur Datenverarbeitung und Aufbereitung, Konsolen Programme zur Ablaufsteuerung und Konsolen CLIs damit erstellt.',
			CoffeeScript: 'Als JavaScript selbst sich wenig weiter entwickelte und als ES5 stecken blieb nutze ich CoffeeScript, dessen Syntax das erstellen komplexer Strukturen vereinfachte. Einige Zeit waren meine Module damit erstellt und wurden in JavaScript in einem build-Schritt umgesetzt.',
			Bash: 'Um kleine Aktionen und Automatisierung auf den Linux Servern durchzuführen ist dies die erste Wahl und zusammen mit den Linux Tools eine leistungsstarke Basis. Damit das arbeiten hiermit einfacher wird habe ich eine eigene Bibliothek erstellt um paralleles arbeiten, postgres Aufrufe und vieles mehr einfach zu erledigen. Bei komplexeren Aufgaben kombiniere ich dies mit NodeJS.',
			SQL: 'Relationale Datenbanken werden in vielen Fällen benötigt, ich schreibe daher häufig einfache bis komplexe Abfragen und Prozeduren um Aktionen und Datenveränderungen direkt auf der Datenbank durchzuführen. Meist hilft dies auch bei der Fehleranalyse. Die Schwierigkeit ist meist nicht die Abfrage, sondern diese performant umzusetzen und dabei auch auf Indizes zu achten.',
			Rust: 'Bis jetzt habe ich die Sprache erlernt und kleine Programme damit erstellt. Im Moment ist der Zeitaufwand um etwas in Rust zu erstellen noch das Manko, und ich greife teils eher zu JavaScript. Für stabile produktive Systeme halte ich dies aber für die aktuell beste Sprache.',
			Java: 'In meiner Zeit an der Universität Tübingen und INMEDEA GmbH war dies meine hauptsächlich genutzte Sprache kombiniert mit JavaScript im Browser. Ich erzeugte ein komplettes Lernsystem mit Java 5, JSP und Servlets.',
			PHP: 'Nachdem Perl sich einige Jahre nicht mehr weiterentwickelt hat wechselte ich auf PHP und lernte das man damit gute Software schreiben kann, wenn man den Code modularisiert und strukturiert gestaltet. Ich habe ein eigenes Content-Management-System und eine Medien Konvertierungssoftware damit erstellt.',
			Python: 'Zunächst habe ich dies einfach nur erlernt, später führte ich damit Bildverarbeitung durch und nutzte es über 2 Jahre an einem Fremdsystem dessen Wartung und Weiterentwicklung ich übernommen habe.',
			Perl: 'Direkt nach meinem Studium war dies, meiner Ansicht nach, die beste Sprache um leistungsfähige Web Anwendungen zu erstellen. Ich erstellte mehrere Content-Management-Systeme damit, besonders gefallen haben mir die Möglichkeiten durch reguläre Ausdrücke Texte zu verarbeiten und die grep, map, pack Methoden auch wenn damit der Code eher unleserlich wurde.',
			Go: 'Ich habe mir die Sprache und Syntax angeeignet aber kein konkretes Projekt damit entwickelt da ich mehr auf JavaScript und Rust setzte.'
		},
		technology: {
			title: 'Technologien',
			Web: 'Während mines Studiums bin ich mit der Web Technologie gewachsen. Von einfachen HTML Seiten über server basierte Dienste, Web Applikationen sind wir heute mit dem Cloud Computing in einer Welt die zentrale daraus besteht. Desktop Anwendungen, Web Anwendungen, Mobile Apps, responsive Design und alles aus einer Code Basis ist das was auch ich entwickle und als Ziel für moderne UIs sehe.',
			Linux: 'Mit dem Beginn meines Studiums kam ich zu Linux und benutze es seither als Server und Desktop Betriebssystem. Ich liebe s und habe über die Zeit an unterschiedlichen Distributionen gearbeitet: Debian, Ubuntu, Mint, RedHat, CentOS, Arch Linux, Manjaro, Gentoo, OSX und Solaris.',
			VMWare: 'Als Administrator sind virtuelle Maschinen das tägliche Brot. Hier habe ich mit ESX Servern und hunderte von VMs mit Zugriff auf NetApp Speicher.',
			Docker: 'Komplexe Software erstelle ich gerne als Docker image, baue diese mit Continuos Integration, prüfe die Security und betreibe damit laufende Container. Soweit möglich erstelle ich Software immer mit und ohne Docker um dem Betreiber die Wahl zu lassen. Auch Kubernetes habe ich mir angeschaut, allerdings bisher noch nicht produktiv betrieben.',
			IaaS: `Um VM basierte Umgebungen automatisiert zu managen arbeite ich mich in den Bereich Infrastructure-as-a-Service ein um sowohl die Updates zu vereinfachen als auch dynamisch auf Lastanstieg reagieren zu können.`
		},
		middleware: {
			title: 'Middleware',
			NginX: 'Wenn es um Geschwindigkeit und hohe Last geht, nutze ich NginX als schnellen Proxy und Loadbalancer.',
			Apache: 'Lange Zeit nutzte ich Apache als leistungsstarken Webserver, der durch seine zahlreichen Module universal einsetzbar ist als HTTP/HTTPS Server, Proxy, Loadbalancer und PHP/Perl Middleware.',
			Tomcat: 'Seit ich mit Java als Server arbeite ist dies der von mir am meist genutzte Anwendungsserver. das management mache ich per Web Interface oder API bei automatischem Deployment.',
			PostgreSQL: 'Die meisten relationalen Datenbanken in meinem Arbeitsumfeld basieren aufgrund der Funkion und der Extensions darauf.',
			MySQL: 'Einige Fremdsysteme nutzen MySQL als Datenbank und ich habe vor Jahren auch hauptsächlich auf MySQL gebaut.',
			MongoDB: 'Seit einiger Zeit benutze ich auch MongoDB als Dokument orientierte Datenbank. Auch in meinem Arbeitsumfeld kommt dies immer mehr in Kombination mit Elasticsearch',
			Joomla: 'Ein Content-Management-System, das ich seit Jahren mit selbst gebauten Layouts bei Vereinen nutze. Meist mit Terminverwaltung, kleinen Shops, Bilder Galerien, News und Video Integration.'
		},
		web: {
			title: 'Web Frameworks',
			Svelte: `Svelte reizte mich vor allem wegen des neuen Ansatzes für Benutzeroberflächen, es kompiliert den Code in ideales JavaScript, anstatt den Anwendungscode zur Laufzeit zu interpretieren. Damit ist es schneller und schlanker im Browser. Die SvelteKit Komponente erlaubt es statische Sites, CSR, SSR und auch hybride Sites zu bauen mit vorberechneten und bereits komprimierten Code Teilen. Diese Site ist eine der ersten Ergebnisse hiervon.`,
			Express: 'Dies ist der von mir am meist genutzte Web Server, nachdem ich auch Hapi, Koa und andere getestet habe.',
			Feathers: 'Dieses Framework nutze ich teils für APIs, da es ein gutes Interface für REST Server mit Hooks im Server und Client erlaubt.',
			Vue: 'Neben Angular und React ist dies eines der großen drei Webclient Frameworks, das ich ansprechender fand und einige kleine Sites damit erstellt habe.',
			Quasar: 'Dies ist ein imposantes Framework, basierend auf VueJS, mit dem ich Web-, Mobile Apps und Desktop Anwendung aus einer Code Basis erstellt habe.',
			Bootstrap: 'JavaScript UI Framework für das einfache erstellen deer Site mittels vorgefertigter CSS Klassen. Hiermit kann leicht ein Responsive Design erstellt werden.',
			JQuery: 'Lange Zeit war dies der quasi Standard bei Websites für dynamische Inhalte und das dynamische Ändern der DOM Struktur.'
		},
		tools: {
			title: 'Tools',
			VSCode: "Da ich oft Programmiersprachen nutze, die selbst nicht primär in einer klassischen IDE unterstützt werden, kam ich von Kate, Sublime, atom zu VSCode. Das ist der derzeit beste Editor, der mit seinen vielen Plugins universal einsetzbar ist und die Basis meines technischen arbeitens bildet.",
			GitLab: 'GitLab nutze ich als universales Management System lokal und in der Cloud. Neben dem Management, Nutzung als Entwickler und Betreiber habe ich eine ausgefeilte CI Pipeline erstellt, die auch einige der Nachteile der freien Version ausgleicht. Von Code checks, Unit-, Integrations Tests über bauen, security check, deployment in unterschiedliche Netze und End-to-End Tests sind modular und erweiterbar auf alle Sprachen und Tools integrierbar.',
			Jira: 'Einige Jahre lang habe ich eine selbst gehostetes Jira installiert, betrieben und genutzt, bis wir dies dann in die Cloud umgezogen haben.',
			Confluence: 'Genau wie bei Jira wurde auch Confluence einige Zeit von mir gemanagt, bis es in die Cloud umgezogen wurde.',
			Mattermost: 'Als interner Chat Server mit Audio Calls habe ich dies für die tägliche Kommunikation installiert und manage den server.',
			Jenkins: 'Ich manage und nutze dies kombiniert mit Subversion und GIT um Java Applikationen zu bauen und auf die Zielsysteme zu verteilen.',
			Prometheus: 'Ich habe die eingeführt um die Metriken von Servern, Services, Datenbanken und Logs über die zeit zu sammeln.',
			Grafana: 'Dies habe ich installiert, eingerichtet und eine vielzahl an Dashboards erstellt mit denen der aktuelle System/Service Zustand sowie die daten und Prozessabläufe dargestellt werden.',
			PRTG: 'Alternatives Monitoring System in einem anderen Rechenzentrum, das stark auf SMTP setzt und vor allem im Windows Bereich stärker ist.',
			Zabbix: 'Als Monitoring System bei meinem Arbeitgeber für die produktiven Systeme genutzt. Analyse über Agent-Skripte und viele Web Szenarios für Website Checks und REST APIs.',
			Nagios: 'Das erste Server Monitoring Tool, das ich genutzt habe.'
		},
		other: {
			title: 'Weiteres',
			learnTitle: 'Lernen',
			learn: 'Ich habe nie aufgehört, neue Technologien zu erlernen, wie in meinem Lebenslauf zu erkennen ist. Alles was sich in meinem Arbeits- und Interessengebiet ist schaue ich mir an, prüfe es und erlerne es, wenn es passt.',
			archTitle: 'Architektur',
			arch: 'in meinen über 25 Arbeitsjahren in der IT habe ich viele Architektur Designs für unterschiedliche Projekte erstellt und erkannt das dies eine Stärke von mir ist. Ich verfolge dabei Konzepte wie "keep it simple", "don\'t reinvent the wheel" und versuche es modular, erweiterbar und dokumentiert zu machen.',
			codeTitle: 'Code Qualität',
			code: 'Ich nutze gerne einen Styleguide als Basis, der mir hilft den Code auch für andere besser lesbar zu machen und halte mich hier an Standards. Auch ist immer ein gewisses minimum an Dokumentation im Code notwendig aber zusätzlich auch ein extra entwickler-/Benutzerhandbuch.',
			solveTitle: 'Problemlösung',
			solve: 'Probleme zu lösen macht Spaß, erst ist es ein komplexes Problem, dann kommen verschiedene Ansätze und wenn man es schließlich gelöst hat freut man sich. Im Betrieb geht es dabei meist um schnelle, pragmatische Lösungen.',
			teamTitle: 'Zusammenarbeit',
			team: "In contrast to my free codings at the work place I always work in teams which helps to don't go in one or the other direction but to find the golden middle way.",
			communicationTitle: 'Kommunikation',
			communication: 'Kommunikation ist der Schlüssel zu guter Teamarbeit, ich spreche gerne Konzepte im Team durch und mache Lösungen durch die Ideen aus der Gemeinschaft noch besser.',
			coordinationTitle: 'Koordination',
			coordination: 'in vielen Projekten hatte ich auch eine projektleiter- oder technische Führungsrolle inne.',
			selfTitle: 'Eigenständigkeit',
			self: 'Je nach Projekt arbeite ich auch selbständig, aber das arbeiten im Team macht mehr Spaß, da man voneinander profitiert und Wissen erweitert. Wenn ich alleine an einem Projekt arbeite mache ich öfter auch Pausen um nach einiger zeit nochmal mit einem frischen Kopf die Lösung noch besser zu machen.',
			langTitle: 'Sprachen',
			lang: 'Ich bin muttersprachlich deutscher, lese und schreibe aber auch gerne im technischen Bereich in Englisch. Meine Dokumentation im Code und auch fast alle technische Dokumente schreibe ich in englisch.'
		},
		more: 'Ich konnte hier gar nicht alles aufschreiben, mit dem ich in den letzten Jahren gearbeitet habe. Es stellt daher nur eine Auswahl der meiner Meinung nach relevanten Punkte dar.'
	},
	projects: {
		menu: 'Projekte',
		title: 'IT Projekte',
		description: 'Beschreibung zu einer Auswahl meiner IT Projekte',
	},
	software: {
		menu: 'Software',
		title: 'Freie Software',
		description: 'Einige freie Open Source Software Komponenten',
	},
	policy: {
		title: 'Impressum und Datenschutz',
		description: 'Impressum und Datenschutz der privaten Site von Alexander Schilling',
		responsible: 'Verantwortliche Person',
		privacy: {
			title: 'Datenschutzerklärung',
			intro:
				'Diese Datenschutzerklärung klärt Sie über die Art, den Umfang und Zweck der Verarbeitung von personenbezogenen Daten (nachfolgend kurz „Daten“) innerhalb unseres Onlineangebotes und der mit ihm verbundenen Webseiten, Funktionen und Inhalte auf (nachfolgend gemeinsam bezeichnet als „Onlineangebot“). Im Hinblick auf die verwendeten Begrifflichkeiten, wie z.B. „Verarbeitung“ oder „Verantwortlicher“ verweisen wir auf die Definitionen im Art. 4 der Datenschutzgrundverordnung (DSGVO).'
		},
		data: {
			title: 'Verarbeitete Daten',
			intro: 'Wir verarbeiten die folgenden Arten von Daten:',
			list: [`Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten).`,
				`Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen).`]
		},
		purpose: {
			title: 'Zweck der Verarbeitung',
			list: [`Zur Verfügungstellung des Onlineangebotes, seiner Funktionen und Inhalte.`,
				`Beantwortung von Kontaktanfragen und Kommunikation mit Nutzern.`,
				`Sicherheitsmaßnahmen.`,
				`Verwaltung der Fremddaten.`]
			//      Reichweitenmessung/Marketing.`
		},
		persons: {
			title: 'Kategorien betroffener Personen',
			list: [`Besucher und Nutzer des Onlineangebotes (Nachfolgend bezeichnen wir diese betroffenen Personen zusammenfassend auch als „Nutzer“).`,
				`In den Fremddaten gespeicherte personenbezogene Daten (siehe hierzu die Datenschutzverordnungen dieser Systeme).`]
		},
		terms: {
			title: 'Verwendete Begriffe',
			list: [`„Personenbezogene Daten“ sind alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person (im Folgenden „betroffene Person“) beziehen; als identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung (z.B. Cookie) oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann, die Ausdruck der physischen, physiologischen, genetischen, psychischen, wirtschaftlichen, kulturellen oder sozialen Identität dieser natürlichen Person sind.`,
				`„Verarbeitung“ ist jeder mit oder ohne Hilfe automatisierter Verfahren ausgeführten Vorgang oder jede solche Vorgangsreihe im Zusammenhang mit personenbezogenen Daten. Der Begriff reicht weit und umfasst praktisch jeden Umgang mit Daten.`,
				`Als „Verantwortlicher“ wird die natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet, bezeichnet.`,
				`„Auftragsverarbeiter“ eine natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die personenbezogene Daten im Auftrag des Verantwortlichen verarbeitet.`]
		},
		legal: {
			title: 'Maßgebliche Rechtsgrundlagen',
			text:
				'Nach Maßgabe des Art. 13 DSGVO teilen wir Ihnen die Rechtsgrundlagen unserer Datenverarbeitungen mit. Sofern die Rechtsgrundlage in der Datenschutzerklärung nicht genannt wird, gilt Folgendes: Die Rechtsgrundlage für die Einholung von Einwilligungen ist Art. 6 Abs. 1 lit. a und Art. 7 DSGVO, die Rechtsgrundlage für die Verarbeitung zur Erfüllung unserer Leistungen und Durchführung vertraglicher Maßnahmen sowie Beantwortung von Anfragen ist Art. 6 Abs. 1 lit. b DSGVO, die Rechtsgrundlage für die Verarbeitung zur Erfüllung unserer rechtlichen Verpflichtungen ist Art. 6 Abs. 1 lit. c DSGVO, und die Rechtsgrundlage für die Verarbeitung zur Wahrung unserer berechtigten Interessen ist Art. 6 Abs. 1 lit. f DSGVO. Für den Fall, dass lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person eine Verarbeitung personenbezogener Daten erforderlich machen, dient Art. 6 Abs. 1 lit. d DSGVO als Rechtsgrundlage.'
		},
		update: {
			title: 'Aktualisierungen der Datenschutzerklärung',
			text:
				'Wir bitten Sie sich regelmäßig über den Inhalt unserer Datenschutzerklärung zu informieren. Wir passen die Datenschutzerklärung an, sobald die Änderungen der von uns durchgeführten Datenverarbeitungen dies erforderlich machen. Wir informieren Sie, sobald durch die Änderungen eine Mitwirkungshandlung Ihrerseits (z.B. Einwilligung) oder eine sonstige individuelle Benachrichtigung erforderlich wird.'
		},
		thirdparty: {
			title: 'Zusammenarbeit mit Auftragsverarbeitern und Dritten',
			list: [`Sofern wir im Rahmen unserer Verarbeitung Daten gegenüber anderen Personen und Unternehmen (Auftragsverarbeitern oder Dritten) offenbaren, sie an diese übermitteln oder ihnen sonst Zugriff auf die Daten gewähren, erfolgt dies nur auf Grundlage einer gesetzlichen Erlaubnis (z.B. wenn eine Übermittlung der Daten an Dritte, wie an Zahlungsdienstleister, gem. Art. 6 Abs. 1 lit. b DSGVO zur Vertragserfüllung erforderlich ist), Sie eingewilligt haben, eine rechtliche Verpflichtung dies vorsieht oder auf Grundlage unserer berechtigten Interessen (z.B. beim Einsatz von Beauftragten, Webhostern, etc.).`,
				`Sofern wir Dritte mit der Verarbeitung von Daten auf Grundlage eines sog. „Auftragsverarbeitungsvertrages“ beauftragen, geschieht dies auf Grundlage des Art. 28 DSGVO.`]
		},
		rights: {
			title: 'Rechte der betroffenen Personen',
			list: [`Sie haben das Recht, eine Bestätigung darüber zu verlangen, ob betreffende Daten verarbeitet werden und auf Auskunft über diese Daten sowie auf weitere Informationen und Kopie der Daten entsprechend Art. 15 DSGVO.`,
				`Sie haben entsprechend. Art. 16 DSGVO das Recht, die Vervollständigung der Sie betreffenden Daten oder die Berichtigung der Sie betreffenden unrichtigen Daten zu verlangen.`,
				`Sie haben nach Maßgabe des Art. 17 DSGVO das Recht zu verlangen, dass betreffende Daten unverzüglich gelöscht werden, bzw. alternativ nach Maßgabe des Art. 18 DSGVO eine Einschränkung der Verarbeitung der Daten zu verlangen.`,
				`Sie haben das Recht zu verlangen, dass die Sie betreffenden Daten, die Sie uns bereitgestellt haben nach Maßgabe des Art. 20 DSGVO zu erhalten und deren Übermittlung an andere Verantwortliche zu fordern.`,
				`Sie haben ferner gem. Art. 77 DSGVO das Recht, eine Beschwerde bei der zuständigen Aufsichtsbehörde einzureichen.`]
		},
		cookies: {
			title: 'Nutzung von Cookies',
			list: [`Als „Cookies“ werden kleine Dateien bezeichnet, die auf Rechnern der Nutzer gespeichert werden. Innerhalb der Cookies können unterschiedliche Angaben gespeichert werden. Ein Cookie dient primär dazu, die Angaben zu einem Nutzer (bzw. dem Gerät auf dem das Cookie gespeichert ist) während oder auch nach seinem Besuch innerhalb eines Onlineangebotes zu speichern. Als temporäre Cookies, bzw. „Session-Cookies“ oder „transiente Cookies“, werden Cookies bezeichnet, die gelöscht werden, nachdem ein Nutzer ein Onlineangebot verlässt und die Sitzung in seinen Browser schließt. In einem solchen Cookie wird z.B. der Anmelde-Staus gespeichert. Als „permanent“ oder „persistent“ werden Cookies bezeichnet, die auch nach dem Schließen des Browsers gespeichert bleiben. So kann z.B. der Login-Status gespeichert werden, wenn die Nutzer diese nach mehreren Tagen aufsuchen.`,
				`Falls die Nutzer nicht möchten, dass Cookies auf ihrem Rechner gespeichert werden, werden sie gebeten die entsprechende Option in den Systemeinstellungen ihres Browsers zu deaktivieren. Gespeicherte Cookies können in den Systemeinstellungen des Browsers gelöscht werden. Der Ausschluss von Cookies kann zu Funktionseinschränkungen dieses Onlineangebotes führen.`]
		},
		hosting: {
			title: 'Hosting / Datenspeicherung',
			list: [`Die von uns in Anspruch genommenen Hosting-Leistungen dienen der Zurverfügungstellung der folgenden Leistungen: Infrastruktur- und Plattformdienstleistungen, Rechenkapazität, Speicherplatz und Datenbankdienste, Sicherheitsleistungen sowie technische Wartungsleistungen, die wir zum Zwecke des Betriebs dieses Onlineangebotes einsetzen.`,
				`Hierbei verarbeiten wir, bzw. unser Hostinganbieter Bestandsdaten, Kontaktdaten, Inhaltsdaten, Vertragsdaten, Nutzungsdaten, Meta- und Kommunikationsdaten von Kunden, Interessenten und Besuchern dieses Onlineangebotes auf Grundlage unserer berechtigten Interessen an einer effizienten und sicheren Zurverfügungstellung dieses Onlineangebotes gem. Art. 6 Abs. 1 lit. f DSGVO i.V.m. Art. 28 DSGVO (Abschluss Auftragsverarbeitungsvertrag).`,
				`Die Datenverarbeitung und Datenspeicherung erfolgt ausschließlich in Deutschland auf deutschen Servern. Ihre Daten werden somit nicht von uns in ein außereuropäisches Land übermittelt. Der Datentransfer zu Ihnen erfolgt dabei ausschließlich über verschlüsselte Verbindungen zum Schutz der Daten und ihrer Privatsphäre.`]
		},
		contact: {
			title: 'Kontaktaufnahme',
			text:
				'Bei der Kontaktaufnahme mit uns (z.B. per Kontaktformular, E-Mail, Telefon oder via sozialer Medien) werden die Angaben des Nutzers zur Bearbeitung der Kontaktanfrage und deren Abwicklung gem. Art. 6 Abs. 1 lit. b) DSGVO verarbeitet. Die Angaben der Nutzer können in einem Customer-Relationship-Management System ("CRM System") oder vergleichbarer Anfragenorganisation gespeichert werden.'
		},
		gravatar: {
			title: 'Abruf von Profilbildern bei Gravatar',
			list: [`Wir setzen innerhalb unseres Onlineangebotes den Dienst Gravatar der Automattic Inc., 60 29th Street #343, San Francisco, CA 94110, USA, ein.`,
				`Gravatar ist ein Dienst, bei dem sich Nutzer anmelden und Profilbilder und ihre E-Mailadressen hinterlegen können. Wenn Nutzer mit der jeweiligen E-Mailadresse auf anderen Onlinepräsenzen (vor allem in Blogs) Beiträge oder Kommentare hinterlassen, können so deren Profilbilder neben den Beiträgen oder Kommentaren dargestellt werden. Hierzu wird die von den Nutzern mitgeteilte E-Mailadresse an Gravatar zwecks Prüfung, ob zu ihr ein Profil gespeichert ist, verschlüsselt übermittelt. Dies ist der einzige Zweck der Übermittlung der E-Mailadresse und sie wird nicht für andere Zwecke verwendet, sondern danach gelöscht.`,
				`Die Nutzung von Gravatar erfolgt auf Grundlage unserer berechtigten Interessen im Sinne des Art. 6 Abs. 1 lit. f) DSGVO, da wir mit Hilfe von Gravatar den Beitrags- und Kommentarverfassern die Möglichkeit bieten ihre Beiträge mit einem Profilbild zu personalisieren.`,
				`Automattic ist unter dem Privacy-Shield-Abkommen zertifiziert und bietet hierdurch eine Garantie, das europäische Datenschutzrecht einzuhalten (https://www.privacyshield.gov/participant?id=a2zt0000000CbqcAAC&status=Active).`,
				`Durch die Anzeige der Bilder bringt Gravatar die IP-Adresse der Nutzer in Erfahrung, da dies für eine Kommunikation zwischen einem Browser und einem Onlineservice notwendig ist. Nähere Informationen zur Erhebung und Nutzung der Daten durch Gravatar finden sich in den Datenschutzhinweisen von Automattic: https://automattic.com/privacy/.`,
				`Wenn Nutzer nicht möchten, dass ein mit Ihrer E-Mail-Adresse bei Gravatar verknüpftes Benutzerbild erscheint, sollten Sie eine E-Mail-Adresse nutzen, welche nicht bei Gravatar hinterlegt ist. Wir weisen ferner darauf hin, dass es auch möglich ist eine anonyme E-Mailadresse zu verwenden, falls die Nutzer nicht wünschen, dass die eigene E-Mailadresse an Gravatar übersendet wird.`]
		},
		delete: {
			title: 'Löschung von Daten',
			text:
				'Die von uns verarbeiteten Daten werden nach Maßgabe der Art. 17 und 18 DSGVO gelöscht oder in ihrer Verarbeitung eingeschränkt. Sofern nicht im Rahmen dieser Datenschutzerklärung ausdrücklich angegeben, werden die bei uns gespeicherten Daten gelöscht, sobald sie für ihre Zweckbestimmung nicht mehr erforderlich sind und der Löschung keine gesetzlichen Aufbewahrungspflichten entgegenstehen. Sofern die Daten nicht gelöscht werden, weil sie für andere und gesetzlich zulässige Zwecke erforderlich sind, wird deren Verarbeitung eingeschränkt. D.h. die Daten werden gesperrt und nicht für andere Zwecke verarbeitet.'
		},
		logs: {
			title: 'Erhebung von Zugriffsdaten und Logfiles',
			list: [`Wir, bzw. unser Hostinganbieter, erhebt auf Grundlage unserer berechtigten Interessen im Sinne des Art. 6 Abs. 1 lit. f. DSGVO Daten über jeden Zugriff auf den Server, auf dem sich dieser Dienst befindet (sogenannte Serverlogfiles). Zu den Zugriffsdaten gehören Name der abgerufenen Webseite, Datei, Datum und Uhrzeit des Abrufs, übertragene Datenmenge, Meldung über erfolgreichen Abruf, Browsertyp nebst Version, das Betriebssystem des Nutzers, Referrer URL (die zuvor besuchte Seite), IP-Adresse und der anfragende Provider.`,
				`Logfile-Informationen werden zur Fehlersuche und aus Sicherheitsgründen (z.B. zur Aufklärung von Missbrauchs- oder Betrugshandlungen) für einige Tage gespeichert und danach gelöscht. Daten, deren weitere Aufbewahrung zu Beweiszwecken erforderlich ist, sind bis zur endgültigen Klärung des jeweiligen Vorfalls von der Löschung ausgenommen.`,
				`Zum Zwecke der Datensicherheit werden die manipulierenden Zugriffe auf Daten mit Nutzer, Zeit und Aktion notiert. Diese bleiben längere Zeit gespeichert, sind aber nur den Administratoren zugänglich.`]
		}
	}
} satisfies Translation

export default de
