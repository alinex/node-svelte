import type { BaseTranslation } from '../i18n-types'

const en = {
	layout: {
		buttons: {
			vita: 'Vita',
			vitaTitle: 'Curriculum vitae, skills, experience',
			works: 'Works',
			worksTitle: 'projects and documentation',
			localeTitle: 'Switch language'
		},
		logoTitle: "Homepage"
	},
	intro: {
		title: 'What is Alinex?',
		description: 'Developer site with personal information and projects by Alexander Schilling',
		paragraph: [
			'This is an artificial name out of the combination of my short name <b>ALEX</b> and <b>IN</b>-ternet.',
			'Firstly I used this only as a working title for all my developments. Altogether I see my open source developments mostly as a learning project, which sometimes brings out something useful. Till now it developed into a modular base system to create individual projects used in production.',
			'But I also use this title as nickname for myself in various online systems and therefore made this personal side also under the Alinex name.',
			'Read more about me and my works here or look at my developments and technical documentations referenced from here.'
		]
	},
	person: {
		menu: 'Person',
		title: 'About myself',
		description: 'Some information about Alexander Schilling',
	},
	vita: {
		menu: 'Vita',
		title: 'Curriculum Vitae and Experiences',
		schoolTitle: 'Education',
		description: 'Timeline of curriculum vitae and IT experiences',
		timelineBusinessDetail: 'Work Project',
		timelinePrivateDetail: 'Personal Project',
		divibib: {
			job: 'IT Administrator and Operator',
			time: 'January 2016 till now',
			company: 'divibib GmbH, D-72764 Reutlingen<br />a subsidiary of <a href="https://www.ekz.de/en/home/">ekz Group</a>',
			description: 'Operation and Administration of external and internal IT systems including firewall, load balancer, ESX servers with more than 200 virtual servers containing more than 800 productive applications, database clusters and storages. Further second-level-support, analysis, data management, documentation, architecture, automation and optimization.',
			cicd: 'Developing of an unique pipeline which automatically creates the matching jobs for each project vor a consistent workflow. From check, build, security analysis till deploy, review and load test win different environments is everything included. And that works for a growing number of languages and tools.',
			management: 'Modular development system to create IT operation tools for system- and data analysis with automatic repair mode to work remote with multiple hosts.',
			grafana: 'Collection of thousand of metrics from systems, services and data analysis in the Prometheus database with visualizing using Grafana dashboards.',
			provider: 'Planning and realization of move to new provider with restructuring. The main goal was to improve reliability and performance.',
			gitlab: 'Setup as private system and migration from subversion. Initiation of Mattermost as private chat system for the internal and external teams.',
			atlassian: 'Administration, management and introduction in the company including use as new intranet.',
			nodetools: 'CoffeeScript tools as operation helper and server management processes.',
			postgres: 'Administration, management and data analyzes.'
		},
		readers: {
			job: 'IT Project Manager, Architect and Operator',
			time: 'October 2011 - December 2015',
			company: '4readers GmbH, D-72764 Reutlingen<br />a subsidiary of <a href="https://www.ekz.de/en/home/">ekz Group</a>',
			description: 'Planing and realization of modern sales platform for digital and physical medias. This included the development of a shop based on Liferay Portal, development of audio/video transformation with streaming of watermarked media files using Wowza Server.',
			media: 'Audio/Video media conversion and management service with work queues. Including work jobs for format conversion, quality adjusting and watermark preparation.',
			facebook: 'For merchandise I made Facebook applications like advent calendars.',
			ruby: 'I took over a system written in Ruby and Python to maintain and extend it.'
		},
		inmedea: {
			job: 'IT Manager and Developer',
			time: 'February 2006 - September 2011',
			description: 'Continuation of the previous university project as company.'
		},
		uni: {
			job: 'Software Developer',
			time: 'August 2002 - January 2006',
			description: 'Architecture and core developer for case based medical learning system.',
			java: 'Java web system using Java servlets and JSP on a Tomcat with a MySQL Database.'
		},
		webhoch3: {
			job: 'Web Developer',
			time: 'May 2001 - July 2002',
			description:
				'Architecture and development of a content management system for medium sized companies.'
		},
		brokat: {
			job: 'Web Developer',
			time: 'October 1998 - May 2001',
			description:
				'Web server operations, development of Content Management Systems and making of company websites in internet and intranet.',
			cms: 'Using Perl with MySQL database as backend for a self designed Content-Management-System building static pages.',
		},

		fh: {
			title: 'Diploma of Economic Computer Science',
			time: 'October 1994 - February 1999',
			company: 'University Reutlingen',
			description: 'Study at Reutlingen University with practical semester at Merckle Ratiopharm, Ulm.',
			sap: 'Consultant for SAP/R3 initiation in middle sized companies.',
			sapr2: 'Image manipulation using ABAP from SAP/R2 at Merckle Ratiopharm.',
			wago: 'Website editing and management as a part time job.'
		},
		kolleg: {
			title: 'Advanced Technical College Entrance Qualification',
			time: 'September 1993 - July 1994'
		},
		wago: {
			title: 'Communication Electronics Engineer',
			time: 'September 1991 - July 1993',
			description: 'Education with focus on radio technology.'
		},
		berufsschule: {
			title: 'General Certificate of Secondary Education',
			time: 'September 1989 - July 1991',
			description: 'Including the first year of apprenticeship.'
		},
		schule: {
			title: 'Certificate of Secondary Education',
			time: 'September 1980 - July 1989',
			description: 'Helping the teacher to learn to handle the new Atari Computers. Technical exam was made with Atari coding to automate using a relay interface.'
		},
		birth: {
			title: 'Birth',
			time: 'January 1974',
			description: 'Reutlingen, Germany'
		},

		it: {
			svelte: 'Web frontend framework for static CSR, SSR and hybrid sites with a focus on speed, simplicity and functionality using SvelteKit to make next generation of my frontends.',
			feathers: `API programming using Feathers JS which enables to build a NodeJS backend with authentication and authorization system.`,
			checkup: 'Monitoring system to make complex tests easy by configuration and get detailed reports and result values. First real product based on the new NodeJS module set: Server, Validator, DataStore...',
			rust: 'Learning to use Rust with small programming examples. But firstly put away in favor to rapid prototyping in JavaScript.',
			quasar: 'Start to use this based on Vue after checking out Angular, React and Vue.',
			es6: 'Try out the new technologies in different NPM modules.',
			node: 'I learned to use these instead of Perl and began to love the synchronic performance.',
			gadgets: 'Development of frontend gadgets and effects for web sites.',
			php: 'Using PHP with MySQL to create dynamic, server rendered pages.',
			javascript: 'Own frontend framework as abstraction layer to leverage the browser incompatibilities further.',
			accessTitle: 'Association Management',
			access: 'With Microsoft Access forms and database a manage system for the local red cross society to administer material and members.',
			crossbrowser: 'Own cross-browser JavaScript library to work with IE and Netscape (IFrame vs Layer...).',
			delphi: 'I made small applications using Delphi (Pascal).',
			web: 'Started to use and grow together with HTML, using Perl on the server side.',
			linux: 'More and more use of Linux OS like Slackware, DLD, Suse, Debian...',
			windows: 'I switched from GEOS to Windows 3.0. I made small applications using Visual Basic and Turbo Pascal.',
			pc: 'My first PC was a 286 with a hard disk on which I used MS-DOS and GEOS as graphical desktop.',
			amiga: 'I got an Amiga 2000 on which I did some music programming and more.',
			computer: 'From Kosmos electronic and computing sets up to a C64 on which I did a bit of basic programming with sprites.'
		}
	},
	skills: {
		menu: 'Skills',
		title: 'Knowledge & Skills',
		description: 'My knowledge and skills in the IT field',
		code: {
			title: 'Languages',
			TypeScript: 'A lot of my modules are now made using TypeScript because it brings more safety and reduce the number of bugs. As more and more third party modules are also using or supporting TypeScript it helps more and more.',
			JavaScript: 'Right from the start I used JavaScript in the Browser, but since NodeJS was stable I also started using it on the server instead of Perl/Python, too. Now I have over 25 years of experience with all the browser specifics over the time and working in the normal functional, object oriented and the new asynchronous style. Besides thee web programming, I also made data processing, transformation and command line tools for process control also with console CLI.',
			CoffeeScript: 'While JavaScript itself was stuck on ES5 and not really going further I used CoffeeScript over it mostly because of the easier syntax. A long time I did my modules with it and because it transpilers to pure JavaScript it was no Problem for others to use my modules.',
			Bash: 'To do small things and automation on the server bash is the first choice because knowing the powerful linux tools it is used to combine them together and get good working scripts very fast. To make it more helpful I also made a Bash Library to make parallel processing, postgres calls and more easy. For more komplex jobs I combined this with NodeJS.',
			SQL: 'Relational databases are needed in a lot of cases and I often write SQL queries, functions which can work within the database fast and use triggers to automate the processing. Often SQL is the base of the problem analysis. The critical part ist to make these performant and have a look at the needed indices.',
			Rust: 'Till now I learned the language and did made some small programs, but no big ones. At the moment it costs me too much time to implement and I not really needed the power it brings over the JavaScript interfaces I did.',
			Java: 'In my time at University Tübingen and INMEDEA that was my major language combined with JavaScript on the CLient. I made the whole systems using Java 5 with JSP and Servlets.',
			PHP: 'After perl was stagnating I switched to PHP and learned that it is also good and powerful if you use it correctly. I made content management systems and a media conversion and management platform with it.',
			Python: 'First I only learned it, later I did some media processing with it and also used it on a System I had to maintain for two years.',
			Perl: 'Just after my study this was, in my opinion, one of the best languages to make powerful applications fast. I created content management systems and other server backends with it. I loved the powerful regular expressions, grep, map and pack methods but learned to make code documentation with it.',
			Go: 'I learned how to work with it and know the syntax but neither did a lot of things because I switched more to JavaScript or Rust.'
		},
		technology: {
			title: 'Technology',
			Web: 'since I started my studies I grew together with thee web technology. Starting with simple HTML pages, server based information sites, web applications we are now in the world of cloud based computing. Desktop and web applications, mobile apps, responsive Design and all out of the same code base is what i develop and see as the goal to modern UIs.',
			Linux: 'Starting with my studies I came to Linux and used it the whole time as server and desktop OS. I love it and over the time I worked on a lot of unix distributions like: Debian, Ubuntu, Mint, RedHat, CentOS, Arch Linux, Manjaro, Gentoo, OSX and Solaris.',
			VMWare: 'As an administrator virtual machines are the base of everything mainly I work on ESX Servers with hundreds of virtual machines and NetApp storages.',
			Docker: 'For complex software I like to create Docker images, building them using continuos integration, checking security and manage the containers of it. As far as possible I always build docker beside a classic installation to let the operator decide. I studies Kubernetes, but did not use it in production till now.',
			IaaS: `To automate the management of VM based computing clouds I am just now looking into tools for Infrastructure-as-a-Service design. My goal ist to simplify updates and dynamically react to an increasing load.`
		},
		middleware: {
			title: 'Middleware',
			NginX: 'For speed and high usage I use NginX as fast proxy and load balancer.',
			Apache: "I used Apache a long time as powerful web server which is an universal tool because of the multiple modules available. I used it as HTTP/HTTPS server, proxy, load balancer and PHP/Perl middleware.",
			Tomcat: 'Since I work with Java on the server this is the most used application server and I worked in maintaining it by Web interface and API in automatic deployments.',
			PostgreSQL: 'Most databases at my work place are postgres because of it powerful function syntax and available extensions.',
			MySQL: 'Some of our third party tools use MySQL as database and I did with older tools some years ago, too.',
			MongoDB: 'Since some time I use MongoDB as document oriented data store and it is coming more and more in my work environment mostly in combination with ElasticSearch.',
			Joomla: 'Content-Management-System which I used for different websites with self-created layout and easy administration for the customer. Including events, small shop, image galleries and news and video integration.'
		},
		web: {
			title: 'Web Frameworks',
			Svelte: `Svelte got my attention because itt uses a new method to build user interfaces, it compiles code into optimal JavaScript instead of doing it at runtime. That results in a smaller and faster application. With SvelteKit it can build static sites, CSR, SSR and also hybrid sites with prerendered and precompressed parts to make it even faster. This Site is one of the first results of this.`,
			Express: 'This is my most used web server in NodeJS but I also tested Hapi, Koa and others over the time, too.',
			Feathers: 'I found that as a good interface, building a REST server with possible hooks on the server and client.',
			Vue: 'Beside Angular and React this is one of the big client frameworks which I found the most appealing and did some smaller sites with it.',
			Quasar: 'This is an imposing Framework based on VueJS, which I used to build Web-, Mobile- and Desktop-Apps all from one codebase.',
			Bootstrap: 'JavaScript framework used for multilingual and responsive websites sometimes with FancyBox for images.',
			JQuery: 'A long time nobody worked on Websites without using JQuery although it is outdated today. A long time it was the bread and butter for a lot of dynamic changes.'
		},
		tools: {
			title: 'Tools',
			VSCode: "Because I often use languages which don't have strong IDE support I came from Kate, Sublime, Atom to VSCode which at the current time is one of the best editors in my opinion. With all the available plugins it can be setup to be the base of any technical work.",
			GitLab: 'GitLab is used by myself as universal management environment local and in the cloud. Beside management, use as developer and operator I developed an universal pipeline, which can compensate the missing features of the free edition. From code checks, unit and integration tests, build, security check, deployment in different network environments  and end to end tests are modular extensible for any language and tool.',
			Jira: 'I installed, operate and used a self hosted Server over a long time before we switched to the cloud.',
			Confluence: 'Like Jira I also managed a self-hosted Confluence which also was switched to the cloud.',
			Mattermost: 'I installed and manage it to have an internal and private chat server with audio calls to help in our daily communication.',
			Jenkins: 'In combination with subversion and git I manage and use this application to build and deploy our Java applications.',
			Prometheus: 'I set this up to collect metrics from all servers, services, Java applications, databases and log files over time.',
			Grafana: 'I did the installation, setup and build a lot of dashboards to visualize the server/service state, data and process progress.',
			PRTG: 'Alternative monitoring system in other datastore. Which use SMTP and is better with Windows environment.',
			Zabbix: 'Monitoring system used on our production systems using agent scripts and a lot of web scenarios to check all the Websites and REST APIs.',
			Nagios: 'I previously used it for server management.'
		},
		other: {
			title: 'Other',
			learnTitle: 'Learning',
			learn: 'I never finished learning new technologies as you see in my vitae. New systems and languages, as far as they are in my working area, I always check out analyze and learn if they are interesting.',
			archTitle: 'Architecture',
			arch: 'In my over 25 years of working in the IT I did a lot of architectural designs and found that a big strength of myself. I follow concepts like "keep it simple" and "don\'t reinvent the wheel" but also try to make it modular, extensible and documented.',
			codeTitle: 'Code Quality',
			code: 'Mostly I look for a ground line, a style guide to follow while coding. A standard helps to make it better readable by others and work in teams. Also every code needs a minimum of inline documentation but also a real user and developer manual written in separate documents.',
			solveTitle: 'Problem Solving',
			solve: 'I often have fun to solve problems. They start complex, I come to multiple solution concepts and after it is done, I am happy. As operator pragmatic and fast solutions are the goal.',
			teamTitle: 'Team work',
			team: "In contrast to my free codings at the work place I always work in teams which helps to don't go in one or the other direction but to find the golden middle way.",
			communicationTitle: 'Communication',
			communication: 'Communication is the key to a good teamwork and high quality. I discuss concept in the team to make the solution far better.',
			coordinationTitle: 'Coordination',
			coordination: 'I did a lot of projects in which I took the technical lead and organization.',
			selfTitle: 'Standalone work',
			self: 'Working on myself is no problem, but I prefer team work because it can benefit of the different thinking and knowledge. If working completely on my own I sometimes take some timeouts in which I do other things before coming back and rethinking my decisions to come to a better solution.',
			langTitle: 'Languages',
			lang: 'I read and write in German (native) and English (good). Most of my technical documentation and all inline documentation is done in english.'
		},
		more: "I couldn't add all that I used and worked with, so see the list above as a selection of the parts I consider as more relevant."
	},
	projects: {
		menu: 'Projects',
		title: 'IT Projects',
		description: 'Description of a selection from my IT projects',
	},
	software: {
		menu: 'Software',
		title: 'Free Software',
		description: 'Some free and open source software components',
	},
	policy: {
		title: 'Imprint and Policy',
		description: 'Imprint of personal site by Alexander Schilling',
		responsible: 'Responsible person',
		privacy: {
			title: 'Privacy statement',
			intro:
				'This privacy statement explains to you the type, scope and purpose of the processing of personal data (hereinafter referred to as "data") within our online offer and the associated websites, functions and contents (hereinafter jointly referred to as "online offer"). With regard to the terms used, such as "processing" or "person responsible", we refer to the definitions in Art. 4 of the General Data Protection Regulation (GDPR).'
		},
		data: {
			title: 'Processed data',
			intro: 'We process the following types of data:',
			list: [`Usage data (e.g. visited websites, interest in content, access times).`,
				`Meta/communication data (e.g. device information, IP addresses).`]
		},
		purpose: {
			title: 'Purpose of processing',
			list: [`Provision of the online offer, its functions and contents.`,
				`Response to contact requests and communication with users.`,
				`Security measures.`,
				`Management of external data.`]
			//      Range measurement/marketing.`
		},
		persons: {
			title: 'Categories of affected persons',
			list: [`Visitors and users of the online offer (In the following, I also refer to these affected persons as "users").`,
				`Personal data stored in the external data (see the data protection regulations of these systems).`]
		},
		terms: {
			title: 'Terms used',
			list: [`"Personal data" means any information relating to an identified or identifiable natural person (hereinafter referred to as "data subject"); an identifiable natural person is one who can be identified, directly or indirectly, in particular by assignment to an identifier such as a name, an identification number, location data, an online identifier (e.g. cookie) or to one or more special features that express the physical, physiological, genetic, psychological, economic, cultural or social identity of that natural person.`,
				`"processing" means any operation carried out with or without the aid of automated procedures or any such series of operations in connection with personal data. The term goes a long way and covers practically every handling of data.`,
				`"Responsible" means the natural or legal person, authority, institution or other body that alone or together with others decides on the purposes and means of processing personal data.`,
				`"Processor" means a natural or legal person, public authority, agency or other body that processes personal data on behalf of the controller.`]
		},
		legal: {
			title: 'Relevant legal bases',
			text:
				'In accordance with Art. 13 GDPR, we inform you of the legal basis of our data processing. If the legal basis is not mentioned in the data protection declaration, the following applies: The legal basis for obtaining consents is Art. 6 para. 1 lit. a and Art. 7 GDPR, the legal basis for processing for the performance of our services and performance of contractual measures as well as for answering inquiries is Art. 6 para. 1 lit. b GDPR, the legal basis for processing to fulfil our legal obligations is Art. 6 para. 1 lit. c GDPR, and the legal basis for processing to protect our legitimate interests is Art. 6 para. 1 lit. f GDPR. In the event that the vital interests of the data subject or another natural person require the processing of personal data, Art. 6 para. 1 lit. d GDPR serves as the legal basis.'
		},
		update: {
			title: 'Updates of the policy statement',
			text:
				'We ask you to inform yourself regularly about the content of our privacy statement. We will adapt the privacy statement as soon as changes in the data processing carried out by us make this necessary. We will inform you as soon as the changes require your cooperation (e.g. consent) or other individual notification.'
		},
		thirdparty: {
			title: 'Collaboration with processors and third parties',
			list: [`If we disclose data to other persons and companies (contract processors or third parties) within the scope of our processing, transmit it to them or otherwise grant them access to the data, this shall only take place on the basis of a legal permission (e.g. if a transmission of the data to third parties, such as payment service providers, in accordance with Art. 6 Para. 1 lit. b GDPR for contract fulfillment is necessary), if you have consented, if a legal obligation provides for this or on the basis of our legitimate interests (e.g. when using agents, web hosts, etc.).`,
				`If we commission third parties with the processing of data on the basis of a so-called "order processing contract", this is done on the basis of Art. 28 GDPR.`]
		},
		rights: {
			title: 'Rights of data subjects',
			list: [`You have the right to request confirmation as to whether the data concerned are being processed and to request information about these data as well as further information and a copy of the data in accordance with Art. 15 GDPR.`,
				`They have correspondingly. In accordance with Article 16 of the GDPR, you have the right to request the completion of data concerning you or the correction of inaccurate data concerning you.`,
				`In accordance with Art. 17 GDPR, you have the right to demand that relevant data be deleted immediately or, alternatively, to demand a restriction on the processing of the data in accordance with Art. 18 GDPR.`,
				`You have the right to request that the data concerning you that you have provided to us be received in accordance with Art. 20 GDPR and to request its transmission to other persons responsible.`,
				`You also have the right, in accordance with Article 77 GDPR, to lodge a complaint with the competent supervisory authority.`]
		},
		cookies: {
			title: 'Use of cookies',
			list: [`"Cookies" are small files that are stored on the user's computer. Different data can be stored within the cookies. A cookie is primarily used to store information about a user (or the device on which the cookie is stored) during or after his or her visit to an online offer. Temporary cookies, or "session cookies" or "transient cookies", are cookies that are deleted after a user leaves an online offer and closes his browser session. In such a cookie, for example, the logon status is stored. Cookies are referred to as "permanent" or "persistent" and remain stored even after the browser is closed. For example, the login status can be saved when users visit it after several days.`,
				`If users do not want cookies to be stored on their computer, they are asked to deactivate the corresponding option in the system settings of their browser. Stored cookies can be deleted in the system settings of the browser. The exclusion of cookies can lead to functional restrictions of this online offer.`]
		},
		hosting: {
			title: 'Hosting / Data storage',
			list: [`The hosting services we use serve to provide the following services: Infrastructure and platform services, computing capacity, storage space and database services, security services and technical maintenance services that we use for the purpose of operating this online offering.`,
				`We or our hosting provider process inventory data, contact data, content data, contract data, usage data, meta- and communication data of customers, interested parties and visitors of this online offer on the basis of our legitimate interests in an efficient and secure provision of this online offer according to Art. 6 Para. 1 lit. f GDPR in conjunction with. Art. 28 GDPR (conclusion of order processing contract).`,
				`Data processing and storage takes place exclusively in Germany on German servers. Your data will therefore not be transmitted by us to a non-European country. Data is transferred to you exclusively via encrypted connections to protect your data and your privacy.`]
		},
		contact: {
			title: 'Contact',
			text:
				'When contacting us (e.g. via contact form, e-mail, telephone or social media), the user\'s details are processed for processing the contact enquiry and its processing in accordance with Art. 6 para. 1 lit. b) GDPR. User information can be stored in a customer relationship management system ("CRM system") or comparable request organization.'
		},
		gravatar: {
			title: 'Retrieval of profile pictures from Gravatar',
			list: [`We use the service Gravatar of Automattic Inc, 60 29th Street #343, San Francisco, CA 94110, USA.`,
				`Gravatar is a service where users can log in and store profile pictures and their e-mail addresses. If users leave contributions or comments with the respective e-mail address on other online presences (above all in blogs), their profile pictures can be displayed next to the contributions or comments. For this purpose, the e-mail address provided by the users is transmitted to Gravatar in encrypted form for the purpose of checking whether a profile has been saved for it. This is the sole purpose of the transmission of the e-mail address and it will not be used for other purposes, but will be deleted thereafter.`,
				`The use of Gravatar is based on our legitimate interests within the meaning of Art. 6 Para. 1 letter f) GDPR, as we offer the possibility of personalising their contributions with a profile picture with the help of Gravatar.`,
				`Automattic is certified under the Privacy Shield Agreement and thus offers a guarantee to comply with European data protection law (https://www.privacyshield.gov/participant?id=a2zt0000000CbqcAAC&status=Active).`,
				`By displaying the images, Gravatar obtains the IP address of the users, as this is necessary for communication between a browser and an online service. For more information on Gravatar's collection and use of the data, please refer to Automattic's privacy policy: https://automattic.com/privacy/.`,
				`If users do not want an image associated with their email address to appear on Gravatar, you should use an email address that is not stored on Gravatar. We would also like to point out that it is also possible to use an anonymous e-mail address if users do not wish their own e-mail address to be sent to Gravatar.`]
		},
		delete: {
			title: 'Deletion of data',
			text:
				'The data processed by us will be deleted or their processing restricted in accordance with Articles 17 and 18 GDPR. Unless expressly stated in this data protection declaration, the data stored by us will be deleted as soon as it is no longer required for its intended purpose and the deletion does not conflict with any statutory storage obligations. If the data are not deleted because they are necessary for other and legally permissible purposes, their processing is restricted. This means that the data is blocked and not processed for other purposes.'
		},
		logs: {
			title: 'Collection of access data and log files',
			list: [`We, or our hosting provider, collect the following data on the basis of our legitimate interests within the meaning of Art. 6 para. 1 lit. f. GDPR data on each access to the server on which this service is located (so-called server log files). Access data includes the name of the accessed website, file, date and time of access, transferred data volume, notification of successful access, browser type and version, the user's operating system, referrer URL (the previously visited page), IP address and the requesting provider.`,
				`Log file information is stored for a few days for troubleshooting and for security reasons (e.g. to detect misuse or fraud) and then deleted. Data whose further storage is required for evidentiary purposes are excluded from deletion until the respective incident has been finally clarified.`,
				`For the purpose of data security, manipulating access to data is recorded with user, time and action. These remain stored for a longer period of time, but are only accessible to the administrators.`]
		}
	}
} satisfies BaseTranslation

export default en
