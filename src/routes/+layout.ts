import { browser } from '$app/environment';
import type { LayoutLoad } from './$types';
import { setLocale } from '$i18n/i18n-svelte';
import { detectLocale } from '$i18n/i18n-util';
import { loadLocaleAsync } from '$i18n/i18n-util.async';
import { queryStringDetector, sessionStorageDetector, navigatorDetector } from 'typesafe-i18n/detectors';

export const load: LayoutLoad = async (event) => {
    if (browser) {
        const deafultLocale = 'de';
        const locale = detectLocale(queryStringDetector, sessionStorageDetector, navigatorDetector) || deafultLocale;
        await loadLocaleAsync(locale);
        setLocale(locale);
    }

    return event.data;
};

// vite configuration
export const prerender = true;
export const trailingSlash = 'always';
