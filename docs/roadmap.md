# Roadmap

No real roadmap at the moment but the idea to checkout the following:

- Bugs
    - [ ] @html translation missing on reload [sveltejs-6832](https://github.com/sveltejs/svelte/issues/6832)
    - [X] ~~*Language menu style*~~ [2023-08-24]
- Select UI Framework
    - [X] ~~*SMUI tryout*~~ [2023-08-08]
    - [X] ~~*svelteUI tryout*~~ [2023-08-23]
    - [ ] skeleton tryout
- i18n
    - [X] ~~*sveltekit-i18n*~~ [2023-08-12]
    - [X] ~~*typesafe-i18n*~~ [2023-08-15]
- alinex.de (svelte-alinex-de)
    - [X] ~~*intro page*~~ [2023-08-30]
      - [X] ~~*width of the intro to screen size*~~ [2023-08-30]h
      - [X] ~~*copyright + imprint*~~ [2023-08-30]
      - [X] ~~*button for vite*~~ [2023-08-30]
      - [X] ~~*Alinex Logo*~~ [2023-09-18]
      - [ ] animation
    - [X] ~~*imprint page*~~ [2023-09-01]
    - [X] ~~*own theme https://www.reddit.com/r/sveltejs/comments/15p6t4w/how_do_i_use_different_themes_with_skeleton_ui/*~~ [2023-09-21]
    - [ ] vite
      - [X] ~~*header*~~ [2023-09-21]
      - [X] ~~*sidebar*~~ [2023-09-21]
      - [X] ~~*content*~~ [2023-09-21]
      - [ ] layout translations
        - [ ] AppRail
        - [ ] title on logo
      - [ ] person
        - [ ] bild
        - [ ] adresse
        - [ ] Slogan
        - [ ] Zukunftstechnologien
        - [ ] blog link
      - [X] ~~*lebenslauf*~~ [2023-09-27]
        - [X] ~~*businessitem*~~ [2023-09-23]
        - [X] ~~*businessdeail*~~ [2023-09-27]
        - [X] ~~*privatedetail*~~ [2023-09-27]
        - [X] ~~*educationitem*~~ [2023-09-27]
        - [X] ~~*educationdetail*~~ [2023-09-27]
      - [ ] fähigkeiten
      - [ ] Projekte
      - [ ] Software
- Documentation
    - [ ] Svelte: Images
    - [ ] skeleton button
    - [ ] Skelton Popup Menu
- Server Application (svelte-demo-ui)
    - [ ] Authentication
    - [ ] Authorization
    - [ ] Postgres
    - [ ] Mongo
- Schwaben-Piranhas
    - [ ] Bild Galerie
    - [ ] Kalender
- Hosting
  - VM 1€ https://www.ionos.de/server/vps 
  - Domain 1,30€

## Ressources

- https://github.com/TheComputerM/awesome-svelte

I18N:

- https://github.com/kaisermann/svelte-i18n/blob/main/docs/Getting%20Started.md

Testing:

- https://testing-library.com/docs/svelte-testing-library/example

Desktop App:

- https://medium.com/@cazanator/tauri-with-standard-svelte-or-sveltekit-ad7f103c37e7

Examples:

- https://github.com/sveltejs/realworld
- https://strapi.io/blog/how-to-create-a-blog-with-svelte-kit-strapi

At first a demo site should be made which may include my personal website [alinex.de](https://alinex.de).

{!docs/assets/abbreviations.txt!}
