# Tailwind CSS

Tailwind CSS is a CSS framework, which does not provide a series of predefined classes for elements such as buttons or tables. Instead, it creates a list of "utility" CSS classes that can be used to style each element by mixing and matching.

## Basic Usage

Against the classical way there you write CSS rules to stylee each element, you style elements by applying pre-existing classes directly in your HTML.

```html
<div class="p-6 max-w-sm mx-auto bg-white rounded-xl shadow-lg flex items-center space-x-4">
</div>
```

!!! important

    For a **responsive design** you should always first setup and check the smallest display, then adjust the larger ones using the size modifiers (see blow).

## Modifiers

Every utility class in Tailwind can be applied conditionally by adding a modifier to the beginning of the class name that describes the condition you want to target like: `<button class="bg-sky-500 hover:bg-sky-700 ...">`.

- Mouse Positon: `hover`, `focus`, `focus-within`, `focus-visible`
- Links: `active`, `visited`, `target`
- List Position: `first-child`, `last-child`, `only-child`, `nth-child(odd)`, `nth-child(even)`
- Element Position: `first-of-type`, `last-of-type`, `only-of-type`
- Element Properties: `empty`, `disabled`, `enabled`, `checked`, `ìndeterminate`
- Input Element: `default`, `required`, `valid`, `invalid`, `in-range`, `out-of-range`, `placeholder-shown`, `autofill`, `read-only`
- Relevant Position: `before`, `after`, `first-letter`, `first-line`
- Input Type: `marker`, `selection`, `file`, `backdrop`, `placeholder`
- Theme: `dark`
- Scren Size: `sm`, `md`, `lg`, `xl`, `2xl`, `min-[…]`, `max-sm`, `max-md`, `max-lg`, `max-xl`, `max-2xl`, `max-[…]`
- Media Display: `portrait`, `landscape`, `motion-safe`, `motion-reduce`, `contrast-more`, `contrast-less`, `print`, `supports-[…]`
- ARIA: `aria-checked`, `aria-disabled`, `aria-expanded`, `aria-hidden`, `aria-pressed`, `aria-readonly`, `aria-required`, `aria-selected`, `aria-[…]`
- Text Direction: `rtl`, `ltr`
- Special: `data-[…]`, `open`

## Define screen sizes

While there are already screen sizes defined for responsiveness, you can define your own break points instead in `tailwind.config.ts`:

```ts
export default {
	...
	theme: {
		screens: {
			'2xs': '360px',
			'xs': '480px',
			'sm': '640px',
			'md': '768px',
			'lg': '1024px',
			'xl': '1280px',
			'2xl': '1536px'
		},
	},
	...
}
```

## Simplify Reuse

Sometimes you define the same classes again and again, like for each element of a list. This tends to look blown. But you can simplify it in Svelte.

### Use Variables

```html
<script>
	import { LL } from '$i18n/i18n-svelte';
	import { Building2Icon } from 'lucide-svelte';
	const style = {
		entry: 'lg:flex items-center lg:space-x-6 mb-3',
		left: 'flex items-center space-x-4 lg:space-x-4 lg:space-x-reverse',
		primary:
			'flex items-center justify-center w-12 h-12 rounded-full bg-primary-500 text-black shadow lg:order-1',
		secondary:
			'flex items-center justify-center w-12 h-12 rounded-full bg-secondary-500 text-black shadow lg:order-1',
		date: 'font-caveat font-medium text-xl text-primary-500 lg:w-72',
		heading: 'text-slate-500 ml-14',
		title: 'text-slate-00 font-bold',
		detail: 'px-4 text-slate-100 shadow ml-10 lg:ml-96'
	};
</script>

<div class="w-full max-w-5xl">
	<div
		class="space-y-8 relative before:absolute before:inset-0 before:ml-4 before:-translate-x-px lg:before:ml-[20.4rem] lg:before:translate-x-0 before:h-full before:w-1 before:bg-gradient-to-b before:from-transparent before:via-slate-100 before:to-transparent"
	>
		<div class="relative">
			<div class={style.entry}>
				<div class={style.left}>
					<div class={style.primary}><Building2Icon /></div>
					<time class={style.date}>{$LL.vita.divibib.time()}</time>
				</div>
				<div class={style.heading}>
					<span class={style.title}>{$LL.vita.divibib.job()}</span>
				</div>
			</div>
			<div class={style.detail}>{$LL.vita.divibib.detail()}</div>
		</div>
    </div>
</div>
```

### Use Components

Especialy, if the same element should be used multiple times, make a Svelte component of it. Now you can use the same component multiple times with different variable parts. I will show it with an example:

First I extract the tailwind style classes into an external file, to use it in multiple variants of a component:

```ts
// lib/timeline/style.ts
export default {
    entry: 'lg:flex items-center lg:space-x-6 mb-3',
    left: 'flex items-center space-x-4 lg:space-x-4 lg:space-x-reverse',
    primary:
        'flex items-center justify-center w-12 h-12 rounded-full bg-primary-500 text-black shadow lg:order-1',
    secondary:
        'flex items-center justify-center w-12 h-12 rounded-full bg-secondary-500 text-black shadow lg:order-1',
    date: 'font-caveat font-medium text-xl text-primary-500 lg:w-72',
    heading: 'text-slate-200 ml-14',
    title: 'text-slate-00 font-bold',
    detail: 'px-4 text-slate-100 shadow ml-10 lg:ml-[22.4rem]'
};
```

Then I make my component:

```html
<!-- lib/timeline/BusinessItem.svelte -->
<script>
	import { Building2Icon } from 'lucide-svelte';
	import style from './style';

	export let time = '';
	export let job = '';
	export let company = '';
	export let detail = '';
</script>

<div class="relative">
	<div class={style.entry}>
		<div class={style.left}>
			<div class={style.primary}><Building2Icon /></div>
			<time class={style.date}>{time}</time>
		</div>
		<div class={style.heading}>
			<div class={style.title}>{job}</div>
			<div>{company}</div>
		</div>
	</div>
	<div class={style.detail}>{detail}</div>
</div>
```

And then I can use it:

```html
<script>
	import { LL } from '$i18n/i18n-svelte';
	import { Building2Icon } from 'lucide-svelte';
	import TimelineBusinessitem from '$lib/timeline/BusinessItem.svelte';
</script>

<div class="w-full max-w-5xl">
	<div
		class="space-y-8 relative before:absolute before:inset-0 before:ml-4 before:-translate-x-px lg:before:ml-[20.4rem] lg:before:translate-x-0 before:h-full before:w-1 before:bg-gradient-to-b before:from-transparent before:via-slate-100 before:to-transparent"
	>
		<TimelineBusinessitem
			time={$LL.vita.divibib.time()}
			job={$LL.vita.divibib.job()}
			company="divibib GmbH"
			detail={$LL.vita.divibib.description()}
		/>
	</div>
</div>
```

{!docs/assets/abbreviations.txt!}
