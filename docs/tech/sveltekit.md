# SvelteKit

SvelteKit is a framework for developing robust, performant web applications using the Svelte component framework, which compiles HTML templates to specialized code that manipulates the DOM directly, which may reduce the size of transferred files and give better client performance. SvelteKit with Svelte is like Next with React.

## Build Variants

SvelteKit can build different deploaments:

- Static Site
- Node Serveer Application
- and more

This framework supports the first two including a docker image. This is don using different scripts in the `package.json`:

```json
	"scripts": {
        ...
		"build": "vite build && cp LICENSE.md package.json build && cp docs/install/node.md build/README.md",
		"build-docker": "npm run build && docker build -t alinex/svelte-app:test .",
		"export": "STATIC=true vite build && cp LICENSE.md build && cp docs/install/static.md build/README.md"
    },
    ...
```

To get this working two different adapters are needed. We include both in the framework and use them depending on the environment variable `STATIC` this is done in the `svelte.config.js`:

```js
import node from '@sveltejs/adapter-node';
import stat from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	...
    kit: {
		adapter: process.env.STATIC
			? // generate static site
			stat({
				// default options are shown. On some platforms
				// these options are set automatically — see below
				pages: 'build',
				assets: 'build',
				fallback: undefined,
				precompress: true,
				strict: true
			})
			: // generate node server
			node({ precompress: true }),
        ...
    }
}
```

## Routing

The routing is based on thee filestructure under `src/routes`.

- each directory is a context path
- `[var]` variable parts in REST syntax are possible
- `[[var]]` the variable can be optional
- `[...var]` multiple level depth
- `[x+nn]` (hex) or `[u+nnnn]` (unicode) is used to match special characters
- `(group)` are not part of the context path but used to make separate lacout groups

More specific routes are higher priority.

Each folder can contain layout files, page, error page and more.

## Layouts

In each folder there can be an `+layout.svelte` file containing the layout. Multiple layouts will be cascaded.
Each layout should contain one `<slot />` there the page or sublayout is imported.

A page can use a specific layout from its path using `@<folder>`: `+page@item.svelte` - inherits from `src/routes/(app)/item/+layout.svelte`.
The same is possible for layouts to break out of its direct parent layout.

- `+layout.svelte` - layout structure
- `+layout@item.svelte` - don't use parent layout as base but specified one
- `+layout@.svelte` - don't use parent layout as base but root layout  
- `+layout.ts` - exports a `load` function to get data
- `+layout.server.ts` - `load` function (only on server)

## Pages

The real content of a route is it's page. 

- `+page.svelte` - page content
- `+page@item.svelte` - don't use parent layout but specified one
- `+page@.svelte` - don't use parent layout but root layout  
- `+page.ts` - exports a `load` function to get data
- `+page.server.ts` - `load` function (only on server)

The page can slso export some options:
- `export const prerender = true` or `false` or `'auto'`
- `export const ssr = true` or `false`
- `export const csr = true` or `false`

## Error Page

If an error occurs during load, this page will be displayed depending on route:

- `+error.svelte` error page 

!!! warn

    `+error.svelte` is not used when an error occurs inside handle or a `+server.js` request handler.

## Server

A `+server.ts` will make API routes possible, which gives you full control over the response. It exports functions corresponding to HTTP verbs like `GET`, `POST`, `PATCH`, `PUT`, `DELETE`, `OPTIONS`, and `HEAD`.

```ts
import { json } from '@sveltejs/kit';
 
/** @type {import('./$types').RequestHandler} */
export async function POST({ request }) {
  const { a, b } = await request.json();
  return json(a + b);
}
```

If it is in the same directory with a `+page.svelte` file it will be called for PUT/PATCH/DELETE/OPTIONS and if the accept header does not include `ttext/html`.

## Hooks

Hooks are app-wide functions you declare that SvelteKit will call in response to specific events, giving you fine-grained control over the framework's behaviour.

- `src/hooks.server.js` - your app's server hooks
- `src/hooks.client.js` - your app's client hooks

They can contain the functions:

- `handle(event, resolve)` - (only server) then the server receives a request
- `handleFetch(request, fetch)` - (only server) allows you to modify (or replace) a fetch request that happens inside a `load` or `action` function
- `handleError(error, event)` - if an unexpected error is thrown during loading or rendering
- 

## State Management

SvelteKit contains different stores which be included them from `$app/stores`:


- `navigating` - a readable store containing with Objects containing: from, to, type
- `page` - containg page data
- `updated` - flag which is `true` if a new vrsion of the app is on the server

## Environment

Include them from `$app/environment`:

- `browser` - flag is `true` if code is run in the browser
- `dev` - flag is `true` if this is the dev server
- `building` - flag is `true` while building or prerendering is done
- `version` - value of `config.kit.version.name`

## Aliases

- `$lib` -> `src/lib`
- `$i18n` -> `src/i18n` (added by [i18n](../dev/i18n.md)

## Service Worker

TODO: Describe usage...

## Helper

Import from `@sveltejs/kit`:

- `error(status, body.message)`
- `fail(status, data)`
- `json(data)` - create a JSON response object
- `text(body)` - create a text only response object
- `redirect(status, location)` - redirect to other URL
- `resolvePath(id, params)`

## Preloading

The preloading can bee triggered to load on `hover` (touchstart) or `tap` (touchstart, mousedown) in `app.html`:

```html
<body data-sveltekit-preload-data="hover">
```

{!docs/assets/abbreviations.txt!}
