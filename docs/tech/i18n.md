title: typesafe i18n

# Multilingual Support

At the time of creating this I analyzed three different libraries:

- [svelte-i18n](https://github.com/kaisermann/svelte-i18n) - no longer maintained
- [sveltekit-i18n](https://github.com/sveltekit-i18n/lib)
- [typesafe-i18n](https://github.com/ivanhofer/typesafe-i18n)

I then decided to use typesafe-i18n because it has active development, a small footprint and full typescript support to help find errors at development time.

## Architecture

### Generators

The [generators](https://github.com/ivanhofer/typesafe-i18n/tree/main/packages/generator) are called using the `typesafe-i18n` command and look for changes that you make to locale files. 
It then will generate the project structure within the `src/i18n` directory.

We can configure generators as per our requirements. Here is the list of available options that can be utilized to customize generators.

### Namespaces

By default translations inside [namespaces](https://github.com/ivanhofer/typesafe-i18n/tree/main/packages/generator#namespaces) are not loaded. You have to manually load them via `loadNamespaceAsync`.

### Detectors

[Locale detection](https://github.com/ivanhofer/typesafe-i18n/tree/main/packages/detectors) is a key part of any i18n solution. Therefore typesafe-i18n provides a solution to detect a user's locale.

Dectotors exports multiple utility methods that enable us to detect locale on the client and server side. We will see a few of them in sections Detect the locale on the server side and Detect the locale on the client side.

### Runtime

The [runtime package](https://github.com/ivanhofer/typesafe-i18n/tree/main/packages/runtime) provides the means of writing and using translations in typesafe-i18n. It exposes wrappers that enable us to pluralize, format and translate our messages.

From amongst many utilities exported by this package, three important ones that I want to highlight are:

i18nString (LLL): string interpolation for selected parts of an application
i18nObject (LL): for frontend-applications or projects that only load a single locale per user
i18n (L): for APIs or backend-applications that need to handle multiple locales

### Formatters

This package exports multiple utility methods useful for [formatting](https://github.com/ivanhofer/typesafe-i18n/tree/main/packages/formatters). 

### Importers:

This package is used for [importing](https://github.com/ivanhofer/typesafe-i18n/tree/main/packages/importer) language files that come from an API, spreadsheet or JSON files.

### Exporters:

This package is used for [exporting](https://github.com/ivanhofer/typesafe-i18n/tree/main/packages/exporter) language files to a service or an API. 

## Inclusion

The generator is included into dev mode by using `npm-run-all` whcih will call:
- `dev`: run vite and typesafe-i18n
- `vite`: run only vite
- `typesafe-i18n`: run only typesafe-i18n

Further the `+layout.ts` will load the translations:

```ts
import { browser } from '$app/environment';
import type { LayoutLoad } from './$types';
import { setLocale } from '$i18n/i18n-svelte';
import { detectLocale } from '$i18n/i18n-util';
import { loadLocaleAsync } from '$i18n/i18n-util.async';
import { queryStringDetector, sessionStorageDetector, navigatorDetector } from 'typesafe-i18n/detectors';

export const load: LayoutLoad = async (event) => {
    if (browser) {
        const deafultLocale = 'de';
        const locale = detectLocale(queryStringDetector, sessionStorageDetector, navigatorDetector) || deafultLocale;
        await loadLocaleAsync(locale);
        setLocale(locale);
    }

    return event.data;
};

// vite configuration
export const prerender = true;
export const trailingSlash = 'always';
```

And in each page you may use the translation method.

To switch translations you may use:

```html
<script lang="ts">
	import { browser } from '$app/environment';
	import { onDestroy, onMount } from 'svelte';
	import { setLocale, locale } from '$i18n/i18n-svelte';
	import { loadLocaleAsync } from '$i18n/i18n-util.async';
  // define display name for each locale
	const localeNames = {
		en: 'English',
		de: 'Deutsch'
	};
  // method to switch locale
	async function handleLocaleChange(event: any) {
		event.preventDefault();
		const value = event?.target?.value;
		await loadLocaleAsync(value);
		setLocale(value);
		sessionStorage.setItem('lang', value);
	}
  // get item from session store
	onMount(() => {
		const valueFromSession = sessionStorage.getItem('lang') || 'en';
		sessionStorage.setItem('lang', valueFromSession);
		console.info('onMount: ', $locale);
	});
  // remove from session store on exit
	onDestroy(() => {
		if (browser) {
			sessionStorage.removeItem('lang');
		}
	});
	// update `lang` attribute
	$: browser && document.querySelector('html')!.setAttribute('lang', $locale);
</script>

<!-- display selection -->
<div class="container__toggle">
  <span>Select: </span>
  <select value={$locale} on:change={handleLocaleChange}>
    {#each locales as l}
      <option value={l}>{localeNames[l]}</option>
    {/each}
  </select>
</div>
```

The language entry in the HTML template will be replaced.

```html
<!DOCTYPE html>
<html lang="%lang%">
...
```

And the server hooks will load all locale translations and set the one prefered by the client.

```ts
import { sequence } from '@sveltejs/kit/hooks';
import { prepareStylesSSR } from '@svelteuidev/core';
import { detectLocale } from '$i18n/i18n-util'
import { loadAllLocales } from '$i18n/i18n-util.sync'
import type { Handle } from '@sveltejs/kit';
import { initAcceptLanguageHeaderDetector } from 'typesafe-i18n/detectors'

loadAllLocales()

const setLanguage: Handle = async ({ event, resolve }) => {
    const locale = detectLocale(initAcceptLanguageHeaderDetector(event.request))
    // replace html lang attribute with correct language
    return resolve(event, { transformPageChunk: ({ html }) => html.replace('%lang%', locale) })
}

export const handle = sequence(prepareStylesSSR, setLanguage);
```

### Directory Structure

To make the files easily accessible in sveltekit $lib we use `"outputPath": "./src/lib/i18n/"` in `.typesafe-i18n.json`.

```
src/
  i18n/
    en/                       # translations for en (base)
      index.ts                # entries
    de/                       # translations for de
      index.ts                # entries
    custom-types.ts           # to define additional types
    formatters.ts             # 
    i18n-types.ts             # auto generated
    i18n-util.async.ts        # auto generated logic to load locales
    i18n-util.sync.ts         # auto generated sync version
    i18n-util.ts              # auto generated wrapper with type information
```

## Translations

### Structure

Key-value pairs:

```ts
const en: BaseTranslation = {
   HI: 'Hello',
   LOGIN: 'click here to login'
   LOGOUT: 'logout'
}
```

Deep structure:

```ts
const en: BaseTranslation = {
   hi: 'Hello',
   auth: {
      login: 'click here to login'
      logout: 'logout'
   }
}
```

### Syntax

!!! note

    The following examples will show the syntax in the translation entry, how to call it and what it will return.
    To concentrat on how it works we use the LLL notation, which directly uses thee given translation entry. Lateer you may use the LL syntax which will take the matching enry for the correct language out of the translation structure.

    ```ts
    LLL('{0} apples and {1} bananas', 3, 7) // => '3 apples and 7 bananas'
    ```

    Is the same logic as usiing it:

    ```ts
    // en/index.ts
    const en = {
      FRUITS: '{0} apples and {1} bananas',
    }
    // de/index.ts
    const de = {
      FRUITS: '{0} Äpfel und {1} Bananen', 
    }
    ```

    ```html
    <!-- +page.svelte -->
    <script>
      import { LL } from '$i18n/i18n-svelte';
      const link = 'https://kit.svelte.dev';
    </script>

    {$LL.APPLES(data)}
    ```

Teext only:

```ts
LLL('Welcome to my site') // => 'Welcome to my site'
```

Arguments:

```ts
// one
LLL('{0} apples', 12) // => '12 apples'
// multiple
LLL('{0} apples and {1} bananas', 3, 7) // => '3 apples and 7 bananas'
// named arguments
LLL('{nrOfApples} apples and {nrOfBananas} bananas', { nrOfApples: 3, nrOfBananas: 7 }) // => '3 apples and 7 bananas'
```

Plural:

```ts
// two forms
LLL('{nrOfApples} {{apple|apples}}', { nrOfApples: 1 }) // => '1 apple'
LLL('{nrOfApples} {{apple|apples}}', { nrOfApples: 2 }) // => '2 apples'
// shorthand
LLL('{nrOfApples} apple{{s}}', { nrOfApples: 0 }) // => '0 apples'
LLL('{nrOfApples} apple{{s}}', { nrOfApples: 1 }) // => '1 apple'
LLL('{nrOfApples} apple{{s}}', { nrOfApples: 5 }) // => '5 apples'
// shorthand only for singular
LLL('{0} weitere{{s|}} Mitglied{{er}}', 0) // => '0 weitere Mitglieder'
LLL('{0} weitere{{s|}} Mitglied{{er}}', 1) // => '1 weiteres Mitglied'
LLL('{0} weitere{{s|}} Mitglied{{er}}', 9) // => '9 weitere Mitglieder'
// three forms
LLL('The list includes {{ no items | an item | ?? items }}', 0) // => 'The list includes no items'
LLL('The list includes {{ no items | an item | ?? items }}', 1) // => 'The list includes an item'
LLL('The list includes {{ no items | an item | ?? items }}', 12) // => 'The list includes 12 items'
// plural with inject
LLL('{{ a banana | ?? bananas }}', 1) // => 'a banana'
LLL('{{ a banana | ?? bananas }}', 3) // => '3 bananas'
// full syntax {{zero|one|two|few|many|other}}
LLL('I have {{zero|one|two|a few|many|a lot}} apple{{s}}', 0) // => 'I have zero apples'
LLL('I have {{zero|one|two|a few|many|a lot}} apple{{s}}', 1) // => 'I have one apple'
LLL('I have {{zero|one|two|a few|many|a lot}} apple{{s}}', 2) // => 'I have two apples'
LLL('I have {{zero|one|two|a few|many|a lot}} apple{{s}}', 6) // => 'I have a few apples'
LLL('I have {{zero|one|two|a few|many|a lot}} apple{{s}}', 18) // => 'I have many apples'
// with key reference
LLL('banana{{nrOfBananas:s}}', { nrOfBananas: 1 }) // => 'banana'
LLL('banana{{nrOfBananas:s}}', { nrOfBananas: 3 }) // => 'bananas'
```

Format:

```ts
// {key|formatter}
LLL('Today is {date|weekday}', { date: Date.now() }) // => 'Today is Friday'
// chaining
LLL('Today is {date|weekday|uppercase}', { date: Date.now() }) // => 'Today is FRIDAY'
LLL('Today is {date|weekday|uppercase|shorten}', { date: Date.now() }) // => 'Today is FRI'
```

Switch-Case:

```ts
// {key | {case1: value1, case2: value2, *: defaultValue}}
LLL('{username:string} added a new photo to {gender|{male: his, female: her, *: their}} stream.', 
  { username: 'John', gender: 'male' })
  // => 'John added a new photo to his stream.'
LLL('{username:string} added a new photo to {gender|{male: his, female: her, *: their}} stream.', 
  { username: 'Jane', gender: 'female' })
  // => 'Jane added a new photo to her stream.'
LLL('{username:string} added a new photo to {gender|{male: his, female: her, *: their}} stream.', 
  { username: 'Alex', gender: 'other' })
  // => 'Alex added a new photo to their stream.'

// yes/no
LLL('Price: ${price:number}. {taxes|{yes: An additional tax will be collected. , no: No taxes apply.}}',
  { price: '999', taxes: 'yes' })
  // => 'Price: $999. An additional tax will be collected.'
LLL('Price: ${price:number}. {taxes|{yes: An additional tax will be collected. , no: No taxes apply.}}',
  { price: '99', taxes: 'no' })
  // => 'Price: $99. No taxes apply.'
```

Typesafe Arguments:

```ts
LLL('Hello {name:string}', 'John') // => ERROR: Argument of type 'string' is not assignable to parameter of type '{ name: string; }'.
LLL('Hello {name:string}', { name: 'John' }) // => 'Hi John'
```

### Formatters

!!! note

    The formatters have to be defined in `src/i18n/formatters.ts` like:

    ```ts
    // to have full intl support in nodejs
    const intl = require('intl')
    intl.__disableRegExpRestore()
    globalThis.Intl.DateTimeFormat = intl.DateTimeFormat

    import type { FormattersInitializer } from 'typesafe-i18n'
    import type { Locales, Formatters } from './i18n-types'
    import { date } from 'typesafe-i18n/formatters'

    export const initFormatters: FormattersInitializer<Locales, Formatters> = (locale: Locales) => {
      const formatters: Formatters = {
        simpleDate: date(locale, { day: '2-digit', month: 'short', year: 'numeric' }),
      }

      return formatters
    }
    ```

Custom:

```js
const formatters = {
   custom: (value) => (value * 4.2) - 7
}

LLL("For input '{0}' I get '{0|custom}' as a result", 100) // => "For input '100' I get '413' as a result"
```

Math:

```js
const formatters = {
   sqrt: (value) => Math.sqrt(value),
   round: (value) => Math.round(value),
}

LLL('Result: {0|sqrt|round}', 5) // => 'Result: 2'
```

Date/Time:

```js
import { date, time } from 'typesafe-i18n/formatters'

const formatters = {
  weekday: date(locale, { weekday: 'long' }),
  timeShort: time(locale, { timeStyle: 'short' })
}

LLL('Today is {0|weekday}', new Date()) // => 'Today is friday'
LLL('Next meeting: {0|timeShort}', meetingTime) // => 'Next meeting: 8:00 AM'
```

Number:

```js
import { number } from 'typesafe-i18n/formatters'

const formatters = {
   currency: number(locale, { style: 'currency', currency: 'EUR' })
}

LLL('Your balance is {0|currency}', 12345) // => 'your balance is €12,345.00'
```

Replace:

```js
import { replace } from 'typesafe-i18n/formatters'

const formatters = {
   noSpaces: replace(/\s/g, '-')
}

LLL('The link is: https://www.xyz.com/{0|noSpaces}', 'super cool product') 
  // => 'The link is: https://www.xyz.com/super-cool-product'
```

Upper-/Lowercase:

```js
import { uppercase } from 'typesafe-i18n/formatters'

const formatters = {
   upper: uppercase,
   lower: lowercase
}

LLL('I said: {0|upper}', 'hello') // => 'I said: HELLO'
LLL('He said: {0|lower}', 'SOMETHING') // => 'He said: something'
```

## Workarounds

A timeout will solve the intial load problem on html containing text:

```html
<script>
	import { LL } from '$i18n/i18n-svelte';
	let lin = '';
	setTimeout(() => (line = $LL.intro.paragraph[0]()), 1);
</script>

<h3>Missing content on load</h3>
<ul>
  <li>{@html $LL.intro.paragraph[0]()}</li>
</ul>

<h3>Working</h3>
<ul>
  <li>{$LL.intro.paragraph[0]()}</li>
  <li>{@html line0}</li>
  <li>{@html "Some text"}</li>
</ul>
```

{!docs/assets/abbreviations.txt!}
