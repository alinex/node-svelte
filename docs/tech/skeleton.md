# Skeleton

After evaluating multiple UIs like SMUI and SvelteUI I came to [Skeleton](https://www.skeleton.dev/) because it is looking fine by extending the standard technologies of tailwind and svelte.

## Basics

## Tailwind extensions

## Svelte extensions

## Utilities

## More Components

- [Use Flowbite Core](https://www.skeleton.dev/blog/skeleton-plus-flowbite)
- https://tailwindcomponents.com/
- https://tailblocks.cc/
- https://merakiui.com/
- https://www.material-tailwind.com/

{!docs/assets/abbreviations.txt!}
