# Directory Structure

The following directories exist (logically ordered):

```
# Source Code
src/                    # source code
  i18n/                 # translations using alias $i18n
    en/                 #   translations for en (base)
      index.ts          #   entries
    de/                 #   translations for de
      index.ts          #   entries
    *.ts                #   auto generated includes
  lib/                  # includes using alias $lib for common modules
    layout/             #   for the layout
    server/             #   for the api routes
  params/               # parametr matcher
  routes/               # route folders are server urls
    xxx/                #   each sub folder can contain another page with layout and all it's components
    (xxx)/              #   layout groups, which are not part of the context path
    +layout.svelte      #   layout for the page
    +layout.ts          #   data loader for layout
    +layout.server.ts   #   data loader which only can run on the server
    +page.svelte        #   defines a pagee of the app
    +page.ts            #   data loader for the page
    +page.server.ts     #   data loader which only can run on the server
    +server.ts          #   api route
    +error.svelte       #   customized error page
    ...                 #   all other files are library files for the above
  app.html              # html template
  app.d.ts              # general typescript definitions
  error.html            # fallback for error page
  hooks.client.ts       # client hoks
  hooks.server.ts       # server hooks
  service-worker.ts     # service worker as proxy or offline support
  index.test.ts         # unit test
node_modules/           # auto generated NodejS modules 
package.json            # package config and 3rd party modules
package-lock.json       # currently used mosule versiions
tsconfig.json           # typescript configuration
svelte.config.js        # svelte and sveltekit configuration
vite.config.ts          # compilation setup
.typesafe-i18n.json

# Static files
static/                 # files which will be present directly in the root folder

# Documentation
docs/                   # documentation in markdown
site/                   # ready to use documentation site
mkdocs.yml              # setup like site structure
README.md               # index for repository
LICENSE.md              # license information
.markdownlint.json      # configuration for linter
.gitlab-ci.yml          # CI pipeline to build documentation

# Tests
test-results/
tests/
playwright.config.ts

# Deploy
bin/                    # binaries for local development
config/                 # configuration for deployment
build/                  # auto generated on build
Dockerfile              # to build a docker image

# Tooling
.gitignore
.npmignore
.eslintignore
.eslintrc.cjs
.npmrc
.prettierignore
.prettierrc
```

{!docs/assets/abbreviations.txt!}
