# CLI Commands

Server Start:

- `npm run dev` - run auto reloading development server
- `npm run build` - build NodeJS SSR server
- `npm run build-docker` - build and make a docker image
- `npm run export` - export as static site
- `npm run preview` - run the version in the build directory locally as a test
- `npm run start` - run the production NodeJS version

Helper Commands:

- `npm run check` - run a code check
- `npm run check:watch` - continuously run code check
- `npm run lint` - run code linter
- `npm run format` - pretty format code
- `npm run stats` - collect statistics for documentation
- `npm run docs` - create HTML documentation

Automatically called Steps:

- `npm run stats` - update statistics for documentation
- `npm run preversion` - check and build before upgrade version
- `npm run postversion` - publish to NPM after version upgrade
- `npm run postpublish` - update documentation after publish

{!docs/assets/abbreviations.txt!}
