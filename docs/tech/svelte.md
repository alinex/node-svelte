# Svelte

Svelte is a front-end component framework and language. It compiles HTML templates to specialized code that manipulates the DOM directly, which may reduce the size of transferred files and give better client performance.

## Components

Each component is defined as a `*.svelte` file, which contains Code and HTML Structure.

- Export makes a value an property accessible by external.
- Assignments are reactive so each time a new value is asigned it will update.
- `$:` before a statementt makes it reactive.
- Prefix `$` is used to access store value.
- A lowercase tag, like `<div>`, denotes a regular HTML element. A capitalised tag, such as `<Widget>`, indicates a component.

## Attributes

- Can be a value with script: `<a href="page/{p}">page {p}</a>`.
- Flags will bee there if value is true: `<button disabled={!clickable}>...</button>`.
- Shorthand for `name={name}` is `{name}`.
- Spread attributes are possible: `<Widget {...things} />`
- `$$props` is an object of all properties passed to component.
- `$$restProps` contains all properties not deeclared with `export`.

## Element Directives

- `on:eventname|modifiers={handler}` - listen to DOM events
- `bind:property={variable}` - two way binding for the attribute
- `bind:group={variable}` - group radio or checkboxes
- `bind:this={dom_node}` - reference to the DOM node
- `class:name={value}` - alternative to set class name
- `style:property={value}` - alternative to set style
- `use:action={parameters}` - action function is called then component is created
- `transition:fn={params}` - transition triggered if element entering or leaving the DOM
- `in:fn={params}` or `out:fn={params}` - custom transitions
- `animate:name={params}` - animation runs then the component is updated

## Logic Blocks

- `{#if expression}...{:else if expression}...{:else}...{/if}`
- `{#each expression as name, index (key)}...{/each}`
- `{#await expression}...{:then name}...{:catch name}...{/await}`
- `{#key expression}...{/key}`

## Special

- `{@html expression}` - to insert html
- `{@debug var1, var2, ..., varN}` - logs values theneveer they change
- `{@const assignment}` - const definition inside of a logic block
- `<slot />` - used in lyouts to mark inclusion part
- `$$slots` - array of slot names passed in from parent
- `<svelte:self>` - rcursive inclusion of componetn itself (in conditional block)
- `<svelte:component this={expression} />` - renders a component dynamically, using the component constructor specified as the `this` property
- `<svelte:element this={expression} />` - rendeers an HTML element dynamically
- `<svelte:window on:event={handler} />` - add listeners to the window object or bind properties
- `<svelte:document on:event={handler} />` - add listeners to the document object
- `<svelte:body on:event={handler} />` - add listeners to the body object
- `<svelte:head>...</svelte:head>` - insert elemeents like title or description to the HMTL head
- `<svelte:options option={value} />` - set per page compiler options
- `<svelte:fragment slot="name">...</svelte:fragment>` - placee content in a named slot without wrapping it in a DOM element

## Lifecycle Functions

- `onMount` - as soon as the element is mounted to the DOM
- `beforeUpdate` - run before a state change
- `afteerUpdate` - run afteer a state change
- `onDestroy` - before thee element is unmounted from the DOM

## Transitions & Animations

Transition: `fade`, `blur`, `fly`, `slide`, `scale`, `draw` and `crossfade`

Animation: `flip`

Easing functions (In/Out/InOut): `back`, `bounce`, `circ`, `cubic`, `elastic`, `expo`, `quad`, `quart`, `quint` and `sine` 

{!docs/assets/abbreviations.txt!}
