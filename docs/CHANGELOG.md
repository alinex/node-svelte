# Last Changes

### Not released (September 2023)

- different layouts in sveltekit
- 

### Not released (August 2023)

- update documentation
- reintegrate framework functions
- static and server build
- renewal of sveltekit and svelte
- tryout of different i18n tools
- integration of typesafe-18n
- tryout of different UI frameworks 
- integration of skeleton with tailwind CSS

### Not released (2021)

- basic admin layout
- build static or SSR through npm call
- base with sveltekit

{!docs/assets/abbreviations.txt!}
