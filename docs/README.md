title: Overview

# Svelte & Co - Work Experience

![svelte icon](https://gitlab.com/uploads/-/system/project/avatar/48144867/image_processing20210621-25858-wtm46d.png){: .right .icon}

## What is specific in Svelte?

Whereas other frameworks like React, Vue and Angular generally add code to your web app to make it work in the user's browser, Svelte compiles the code that you write when you build your app. In doing so, it creates very small files and fast websites. [SolidJS](https://www.solidjs.com/) is another framework using the same concept but more with React Syntax in source files and at the moment to new with a lack of support.

I want to use it as:

1. static site generation for my homepage and other pages
2. administration panel using an api server

## Goals

This is a framework to make a svelte application easy, but with proper setup and configuration to reach all goals.
Additionally this will show what is possible as a work experience.

I try to use it for static sites, server and the combination of both.

### Usability

It should be easy usable by endusers, meaning it should extend existing behaviour, learned knowledge and be simple and easy to use wihout explanation. Also it should be possible to be used by handicapped peaple.

**Skeleton with Tailwind** is used as UI framework. The decision was made after analyzing some of the multiple Svelte toolkits available. It is a good basis with a commonly used base systeem which can handle all the quirks for a modern UI.

**Accessibility** whiche this is not my main focus I will always try to add all the neccessary meta information for optimal support.

### Portability

The user interface is not designed for a specific Browser and will run on all Browsers wiith any screen size.
Display in mobile Browsers are possible and nattive mobile applications may also be made later.

**Browser Compatibility:** Based on th used frameworks the design elements are tested on the latest stable versions of Chrome, Firefox, Edge, and Safari. But it does not support any version of IE, including IE 11.

### Performance

It should be as fast as possible for the user. A millisecond is not much but if it is some milliseconds faster it is noticeable to the user.

**Prerendered:** A lot of pages consist of statistic data which don't change for each user and therefor will be stored prerendered by compile time on the server.

**Compression:** You should not send content uncompress and it doesn’t make sense to re-compress static assets upon request. Instead, static assets should be compressed upon deploy like done here by storing
- the uncompressed file
- a gzip compressed version
- and a brotli compressed version

**Preload:** Before the browser registers that the user has clicked on a link, it will detect that you hovered the mouse over it (on desktop) or that a touchstart or mousedown event was triggered. In both cases, it will get a head start on importing the code and fetching the page's data, which can give us an extra couple of hundred milliseconds — the difference between a user interface that feels laggy and one that feels snappy.

Icons are mostly used as inline SVG graphics which prevent additional requests for the font.

**Hydration:** This framework uses SvelteKit to render the pages on the server (SSR) first to show it fast in the Browser. In the client the html will be hydrated, doing further changes to the page using JavaScript and API calls (CSR). This makes the further interaction faster.

### Security

Security means the protection of data, especially personal data. But also be prevented against attackers.

**Up-to-date:** As long as I use something I will keep their dependencies up to date. This ensures new security fixes are applied regardless of if they are announced or not.

**GDPR:** The site should protect the user after at least the GDPR standards. Therefore fonts are included and not loaded from any CDN like Google & Co.

**SSL Protection:** This is not a topic of development but everything is ready to run it in production under full SSL protection.

### Scalability

If used as high availablity service the scalability is a must have to not be overloaded and go down on too much use.

**Server Custer:** As long as a static site is used it can be served through a Nginx cluster which can be sized for any load.
The server should be stateless and can also be served through a cluster behind a load balancer.
If state matters it has to be storeed on the client or in a central database like Redis.

**Kubernetes:** Also the application can be run in a Kubernetes environment which will automatically start new containers as the load increases.

### Maintainability

To make the maintain process easy the code is clearly structured and modularized.

**TypeScript:** As we completely use TypeScript, we gain the power of type safety and auto completion in IDE.

**Testing:** We use linting and E2E testing.

### Extensibility

In the NodeJS environment a ton of modules exist, which allow to extend functionality easily in any direction. Parts like mathematical calculation can also be transferred to othr API servers which can be easily integrated.

## Architecture

This framework uses the following tools:

- [Sveltekit](https://kit.svelte.dev/)
    - [TypeScript](https://www.typescriptlang.org/)
    - [Svelte](https://svelte.dev/)
    - [Vite](https://vitejs.dev/guide/)
- [Typesafe I18N](https://github.com/ivanhofer/typesafe-i18n/blob/main/README.md)
- [Skeleton](https://skeleton-docs-git-v2-skeleton-labs.vercel.app/)
    - [Tailwind CSS](https://tailwindcss.com/)
    - [PostCSS](https://postcss.org/)
- [Alinex MkDocs Implementation](https://alinex.gitlab.io/env/mkdocs.html)

## Features

Currently the following features are implemented and tested:

-   [Multilingual Sites](dev/i18n.md) 
-   [Static Site Export](https://khromov.se/the-missing-guide-to-understanding-adapter-static-in-sveltekit/)

Further you add:

-   [Authentication](https://authjs.dev/reference/sveltekit)
-   [Sentry](https://docs.sentry.io/platforms/javascript/guides/sveltekit/) error-tracking platform
    This will help monitor live applications with a free developer account at https://sentry.io/signup/.
    This can also be  used with a self hosted [GlitchTip](https://glitchtip.com/).
-   Analytics using [Unami](https://umami.is/)
-   TypeScript OpenAPI Client [Apity](https://github.com/cocreators-ee/apity)

{!docs/assets/stats-pdf-license.txt!}

{!docs/assets/abbreviations.txt!}
