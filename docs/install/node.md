title: NodeJS SSR

# Alinex Svelte Experience

This is a NodeJS build of the Alinex Svelte package. You need to run it using node.

## Installing

Copy this build folder onto your server and install node_modules:

```bash
npm ci --production
```

If you run it on a Linux System you may need a system.d start script as `/etc/systemd/system/my-app.service`:

```ini
[Unit]
Description=My Application
After=network.target

[Service]
Type=simple
User=admin
WorkingDirectory=/path/to/build/directory
Environment=NODE_ENV=production
#Environment=HOST=localhost
Environment=Port=3000
ExecStart=/usr/bin/node .
Restart=always
RestartSec=10

[Install]
WantedBy=multi-user.target
```

## Starting the Server

The best way is to use the service from above:

```bash
sudo systemctl start my-app.service
```

But you can also start it manually using:

```bash
NODE_ENV=productionv HOST=localhost PORT=3000 node .
```
