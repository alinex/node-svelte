title: Docker SSR

# Alinex Svelte Experience

This is a Docker image of the Alinex Svelte package. You can run it multiple ways.

The following examples use the docker image with the tag set by `npm run build-docker`: alinex/svelte-app:test

## docker

The simple way is to directly run it though docker:

```bash
# just run the container
docker run -d -p 3000:3000 alinex/svelte-app:test
# stop daemon
docker stop $(docker ps | grep alinex/svelte-app:test | sed 's/ .*//')
```

This will start the docker image as a daemon container using port 3000 locally to map to the docker application also on port 3000.

## docker-compose

Another alternative is to use `docker-compose.yml`:

```yml
# docker-compose.yml

version: '3'

services:
  external:
    image: alinex/svelte-app:test
    build:
      context: .
      dockerfile: Dockerfile
    user: 'node'
    environment:
      # Internal server error messages will not send stacktrace to the browser in production
      - NODE_ENV=production
      # Sets the timezone of the containers OS
      - TZ=Europe/Berlin
    # Points to a file with the sensitive environment variables
    env_file:
      - .env
    restart: unless-stopped
    # Select the port to run on
    ports:
      - 3000:3000
```

Within the folder of this file call `docker-compose up -d` to run it as a daemon.
