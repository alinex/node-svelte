title: Static Site

# Alinex Svelte Experience

This is a static build of the Alinex Svelte package. It is prerendered and can be run from a static webserver.

## Installing

To run it within an Nginx, Apache or any other webserver, copy this directory to the VSite root folder.
That's all, no special setup is needed. So after this you may call it directly in your browser and see it work.

### Nginx

Nginx is simple, fast and comes with all you need:

```
server {
    listen 80;
    server_name www.example.com;
    root /path/to/build;
    
    location / {
        index index.html; 
        try_files $uri $uri/ /index.html =404;
        # use pre compressed
        gzip_static on;
        #brotli_static on;
        # no caching
        expires -1;
    }    
    location ^~ /_app/immutable/ {
        # allow full caching, because files contains a version hash
        expires max;
        add_header Pragma "public";
        add_header Cache-Control "public";
    }
}
```

Brotli support is in the example above deactivated, because it is not included by default in nginx.
[Add Brotli in Ubuntu 20.04](https://www.atlantic.net/dedicated-server-hosting/how-to-install-brotli-module-for-nginx-on-ubuntu-20-04/)

### Apache

A basic sattic website:

```
ServerAdmin contact@example.com
ServerName example.com
ServerAlias www.example.com

DocumentRoot /path/to/build
```

But to also support the precompressed files using gzip (more complex than nginx):

```
ServerAdmin contact@example.com
ServerName example.com
ServerAlias www.example.com

DocumentRoot /path/to/build

# AddEncoding allows you to have certain browsers uncompress information on the fly.
AddEncoding gzip .gz

# Serve gzip compressed HTML files if they exist and the client accepts gzip.
RewriteCond %{HTTP:Accept-encoding} gzip
RewriteCond %{REQUEST_FILENAME}\.gz -s
RewriteRule ^(.*)\.html $1\.html\.gz [QSA]
# Serve gzip compressed CSS files if they exist and the client accepts gzip.
RewriteCond %{HTTP:Accept-encoding} gzip
RewriteCond %{REQUEST_FILENAME}\.gz -s
RewriteRule ^(.*)\.css $1\.css\.gz [QSA]
# Serve gzip compressed JS files if they exist and the client accepts gzip.
RewriteCond %{HTTP:Accept-encoding} gzip
RewriteCond %{REQUEST_FILENAME}\.gz -s
RewriteRule ^(.*)\.js $1\.js\.gz [QSA]

# Serve correct content types, and prevent mod_deflate double gzip.
RewriteRule \.html\.gz$ - [T=text/html,E=no-gzip:1]
RewriteRule \.css\.gz$ - [T=text/css,E=no-gzip:1]
RewriteRule \.js\.gz$ - [T=text/javascript,E=no-gzip:1]
```
