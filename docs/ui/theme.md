# Theme

## Dark Mode

The dark mode is already fully supported in Tailwind as a seeparate theme.

You define the styles with Tailwind using the prefix `dark:`.

A switch can be implemented as:

```html
<script lang="ts">
	import {LightSwitch} from '@skeletonlabs/skeleton';
</script>

<LightSwitch />
```

To make parts specific to current mode you can use:

```html
<script lang="ts">
	import {modeCurrent} from '@skeletonlabs/skeleton';
	import logoDark from '$lib/layout/logo-dark.png';
	import logoLight from '$lib/layout/logo-light.png';
</script>

{#if $modeCurrent}
	<img src={logoLight} alt="Alinex" />
{:else}
	<img src={logoDark} alt="Alinex" />
{/if}
```

## Colors

The theme and it's colors can be defined using the [Generator](https://www.skeleton.dev/docs/generator) or edit them directly in the theme `*-theme.ts`.

```css
// =~= Theme On-X Text Colors =~=
"--on-primary": "0 0 0",
"--on-secondary": "0 0 0",
"--on-tertiary": "0 0 0",
"--on-success": "255 255 255",
"--on-warning": "0 0 0",
"--on-error": "255 255 255",
"--on-surface": "255 255 255",
// =~= Theme Colors  =~=
// primary | #e5a50a 
"--color-primary-50": "251 242 218", // #fbf2da
"--color-primary-100": "250 237 206", // #faedce
"--color-primary-200": "249 233 194", // #f9e9c2
"--color-primary-300": "245 219 157", // #f5db9d
"--color-primary-400": "237 192 84", // #edc054
"--color-primary-500": "229 165 10", // #e5a50a
"--color-primary-600": "206 149 9", // #ce9509
"--color-primary-700": "172 124 8", // #ac7c08
"--color-primary-800": "137 99 6", // #896306
"--color-primary-900": "112 81 5", // #705105
// secondary | #309fa7 
"--color-secondary-50": "224 241 242", // #e0f1f2
"--color-secondary-100": "214 236 237", // #d6eced
"--color-secondary-200": "203 231 233", // #cbe7e9
"--color-secondary-300": "172 217 220", // #acd9dc
"--color-secondary-400": "110 188 193", // #6ebcc1
"--color-secondary-500": "48 159 167", // #309fa7
"--color-secondary-600": "43 143 150", // #2b8f96
"--color-secondary-700": "36 119 125", // #24777d
"--color-secondary-800": "29 95 100", // #1d5f64
"--color-secondary-900": "24 78 82", // #184e52
// tertiary | #62a0ea 
"--color-tertiary-50": "231 241 252", // #e7f1fc
"--color-tertiary-100": "224 236 251", // #e0ecfb
"--color-tertiary-200": "216 231 250", // #d8e7fa
"--color-tertiary-300": "192 217 247", // #c0d9f7
"--color-tertiary-400": "145 189 240", // #91bdf0
"--color-tertiary-500": "98 160 234", // #62a0ea
"--color-tertiary-600": "88 144 211", // #5890d3
"--color-tertiary-700": "74 120 176", // #4a78b0
"--color-tertiary-800": "59 96 140", // #3b608c
"--color-tertiary-900": "48 78 115", // #304e73
// success | #0f6d00 
"--color-success-50": "219 233 217", // #dbe9d9
"--color-success-100": "207 226 204", // #cfe2cc
"--color-success-200": "195 219 191", // #c3dbbf
"--color-success-300": "159 197 153", // #9fc599
"--color-success-400": "87 153 77", // #57994d
"--color-success-500": "15 109 0", // #0f6d00
"--color-success-600": "14 98 0", // #0e6200
"--color-success-700": "11 82 0", // #0b5200
"--color-success-800": "9 65 0", // #094100
"--color-success-900": "7 53 0", // #073500
// warning | #f6d32d 
"--color-warning-50": "254 248 224", // #fef8e0
"--color-warning-100": "253 246 213", // #fdf6d5
"--color-warning-200": "253 244 203", // #fdf4cb
"--color-warning-300": "251 237 171", // #fbedab
"--color-warning-400": "249 224 108", // #f9e06c
"--color-warning-500": "246 211 45", // #f6d32d
"--color-warning-600": "221 190 41", // #ddbe29
"--color-warning-700": "185 158 34", // #b99e22
"--color-warning-800": "148 127 27", // #947f1b
"--color-warning-900": "121 103 22", // #796716
// error | #a51d2d 
"--color-error-50": "242 221 224", // #f2dde0
"--color-error-100": "237 210 213", // #edd2d5
"--color-error-200": "233 199 203", // #e9c7cb
"--color-error-300": "219 165 171", // #dba5ab
"--color-error-400": "192 97 108", // #c0616c
"--color-error-500": "165 29 45", // #a51d2d
"--color-error-600": "149 26 41", // #951a29
"--color-error-700": "124 22 34", // #7c1622
"--color-error-800": "99 17 27", // #63111b
"--color-error-900": "81 14 22", // #510e16
// surface | #003a67 
"--color-surface-50": "217 225 232", // #d9e1e8
"--color-surface-100": "204 216 225", // #ccd8e1
"--color-surface-200": "191 206 217", // #bfced9
"--color-surface-300": "153 176 194", // #99b0c2
"--color-surface-400": "77 117 149", // #4d7595
"--color-surface-500": "0 58 103", // #003a67
"--color-surface-600": "0 52 93", // #00345d
"--color-surface-700": "0 44 77", // #002c4d
"--color-surface-800": "0 35 62", // #00233e
"--color-surface-900": "0 28 50", // #001c32
```
## Google Fonts

To use foreign fonts you should always include them on your site and not load them from a CDN for security reasons like needed for the GDPR. This is easily possible for Google fonts.

### Manual inclusion

1. Download a font as WOFF file from [Google](https://fonts.google.com/) or other sites.
2. If only TTF file, convert it to WOFF using something like: [TTF2WOFF](https://www.fontconverter.io/de/ttf-zu-woff)
3. Copy the `*.woff` file into `static/fonts`
4. Add the font in global style like `src/app.postcss`:

	```css
	@font-face {
		font-family: 'OpenSans';
		src: url('/fonts/OpenSans-VariableFont_wdth,wght.woff');
	}
	```

5. Set the font family globally within your tailwind theme `*-theme.ts`:
	```css
	--theme-font-family-base: 'OpenSans', sans-serif;
    --theme-font-family-heading: 'Oswald', sans-serif;
	```

6. Preloading fonts to avoid your page flickering during hydration in `app.html`:

	```html
	<link
		rel="preload"
		href="%sveltekit.assets%/fonts/OpenSans-VariableFont_wdth,wght.woff"
		as="font"
		type="font/woff"
		crossorigin
	/>
	```

### Fontsource

We use https://fontsource.org here:

1. search for the font on the https://fontsource.org/ site
2. select the font (best use "variable" veersion)
3. click onto `install` button to get instructions
4. Include it using:

	```bash
	npm install -D @fontsource-variable/caveat
	```

5. Now use it in your svelte component:

	```html
	<script>
		import '@fontsource-variable/caveat';
	</script>

	<p><em>Alexander Schilling</em></p>

	<style>
		p {
			font-size: 28px;
			font-family: 'Caveat Variable', sans-serif;
		}
	</style>
	```

{!docs/assets/abbreviations.txt!}
