# Icons

There is no specific icon library to use, it is free for everyone to choose any icon library. The inclusion is possible in different ways, ttoo.

So here are some possibilities. Feel free to select which one to use, but don't mix to much because your users may be disturbed from the different design principles.

## Local SVG Component

To do this you need to have the SVG icon code and store it locally within the `static/`, `lib/` or beside the layout/page as i.e. `IconDE.svelte`.

```html
<script>
	// SVG source from node-modules/flag-icons/flags/4x3
	// extended to make size customizable
	export let size = 15;
</script>

<svg
	width={Math.round((size / 3) * 4)}
	height={size}
	xmlns="http://www.w3.org/2000/svg"
	id="flag-icons-de"
	viewBox="0 0 640 480"
	{...$$restProps}
>
	<path fill="#ffce00" d="M0 320h640v160H0z" />
	<path d="M0 0h640v160H0z" />
	<path fill="#d00" d="M0 160h640v160H0z" />
</svg>
```

Now it can be imported into the `+layout.svelte`:

```html
<script lang="ts">
	import FlagDE from '$lib/layout/IconDE.svelte';
</script>

<button on:click={() => changeLocale(l)} class={classesActive(l)}>
    <svelte:component this={flagDE} />
    <span class="flex-auto">Deutsch</span>
</button>
```

In the example above we copied the SVG source code from [flag-icons](https://github.com/lipis/flag-icons#readme), first.

## Svelte SVG Components

### Lucide

[Lucide](https://lucide.dev/guide/packages/lucide-svelte) is a comunity library of [over 1000 icons](https://lucide.dev/icons/?focus=).

First install it:

```bash
npm install --save-dev lucide-svelte
```

Inside a Svelte file import any of the icons like this:

```html
<script>
  import { Skull } from 'lucide-svelte';
</script>

<Skull />
```

The following prperties can be set:

- `size`	number	24
- `color`	string	currentColor
- `strokeWidth`	number	2
- `absoluteStrokeWidth`	boolean	false

### Flowbite

[Flowbite](https://flowbite-svelte.com/docs/extend/icons) is a collectiion of [over 430 icons](https://flowbite.com/icons/).

First install it:

```bash
npm install --save-dev flowbite-svelte-icons
```

Inside a Svelte file import any of the icons like this:

```html
<script>
  import { CalendarWeekSolid } from 'flowbite-svelte-icons';
</script>

<CalendarWeekSolid />
```

Properties to customize and update the icons.

Outline:

-   size: `xs` | `sm` | `md` | `lg` | `xl` = `md`;
-   role: string = `img`;
-   strokeLinecap: `round` | `inherit` | `butt` | `square` | null | undefined = `round`;
-   strokeLinejoin: `round` | `inherit` | `miter` | `bevel` | null | undefined = `round`;
-   strokeWidth = `2`;
-   ariaLabel = `icon file name`;

Solid:

-   size: `xs` | `sm` | `md` | `lg` | `xl` = `md`;
-   role: string = `img`;
-   ariaLabel = `icon file name`;

Use the class prop to change size, colors and add additional css using tailwind:

```html
<AddressCardSolid class="h-24 w-24 text-blue-700 mr-4" />
```

### Radix

Another library is the [radix-icons-svelte](https://github.com/Brisklemonade/radix-icons-svelte) containing [225 UI Icons(https://icones.js.org/collection/).

First install it:

```bash
npm install --save-dev radix-icons-svelte
```

Then use it in any svelte file:

```html
<script lang="ts">
	import { Globe } from 'radix-icons-svelte';
</script>

<button
    type="button"
    class="btn-icon variant-filled"
    title="Language Switch"><Globe /></button
>
```

## SVG using CSS

### Country Flag Icons

You can either download the whole project as is or install it via npm or Yarn:

```bash
npm install --save-dev flag-icons
```

Include the CSS within your CSS file i.e. `app.postcss`:

```css
import "/node_modules/flag-icons/css/flag-icons.min.css";
```

Then use it like:

```html
<!-- inline within text -->
<span class="fi fi-de"></span>
<!-- use squared version -->
<span class="fi fi-de fis"></span>
<!-- to use it on other elements-->
<div class="fib fi-de"></div>
```

### Fontawesome Free

First you need to download th source, this is done easily as npm package:

```bash
npm install --save-dev @fortawesome/fontawesome-free
```

Next you have to copy the javascript files to a static folder `/fontawesome-free` and load the JavaScript within `app.html`:

```html
<head>
  <!-- Our project just needs Font Awesome Solid + Brands -->
  <script defer src="/fontawesome-free/js/brands.js"></script>
  <script defer src="/fontawesome-free/js/solid.js"></script>
  <script defer src="/fontawesome-free/js/fontawesome.js"></script>
</head>
```

Then you may use it like described on the Fontawesome page:

```html
<body>
  <i class="fa-solid fa-user"></i>
  <!-- uses solid style -->
  <i class="fa-brands fa-github-square"></i>
  <!-- uses brand style -->
</body>
```

## Icon Fonts

### Fontawesome Free

First you need to download th source, this is done easily as npm package:

```bash
npm install --save-dev @fortawesome/fontawesome-free
```

Include the CSS within your CSS file i.e. `app.postcss`:

```css
import "/node_modules/@fortawesome/fontawesome-free/css/fontawesome.min.css";
import "/node_modules/@fortawesome/fontawesome-free/css/brands.min.css";
import "/node_modules/@fortawesome/fontawesome-free/css/solid.min.css";
```

Then you may use it like described on the Fontawesome page:

```html
<body>
  <i class="fa-solid fa-user"></i>
  <!-- uses solid style -->
  <i class="fa-brands fa-github-square"></i>
  <!-- uses brand style -->
</body>
```

## Misc

Other solutions may be:

- https://cweili.github.io/svelte-fa/


{!docs/assets/abbreviations.txt!}
