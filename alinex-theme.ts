import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';

export const alinexTheme: CustomThemeConfig = {
    name: 'alinex-theme',
    properties: {
        // =~= Theme Properties =~=
        "--theme-font-family-base": "OpenSans",
        "--theme-font-family-heading": "Oswald",
        "--theme-font-color-base": "0 0 0",
        "--theme-font-color-dark": "255 255 255",
        "--theme-rounded-base": "9999px",
        "--theme-rounded-container": "8px",
        "--theme-border-base": "2px",
        // =~= Theme On-X Text Colors =~=
        "--on-primary": "0 0 0",
        "--on-secondary": "0 0 0",
        "--on-tertiary": "0 0 0",
        "--on-success": "255 255 255",
        "--on-warning": "0 0 0",
        "--on-error": "255 255 255",
        "--on-surface": "255 255 255",
        // =~= Theme Colors  =~=
        // primary | #e5a50a 
        "--color-primary-50": "251 242 218", // #fbf2da
        "--color-primary-100": "250 237 206", // #faedce
        "--color-primary-200": "249 233 194", // #f9e9c2
        "--color-primary-300": "245 219 157", // #f5db9d
        "--color-primary-400": "237 192 84", // #edc054
        "--color-primary-500": "229 165 10", // #e5a50a
        "--color-primary-600": "206 149 9", // #ce9509
        "--color-primary-700": "172 124 8", // #ac7c08
        "--color-primary-800": "137 99 6", // #896306
        "--color-primary-900": "112 81 5", // #705105
        // secondary | #309fa7 
        "--color-secondary-50": "221 237 245", // #ddedf5
        "--color-secondary-100": "209 232 242", // #d1e8f2
        "--color-secondary-200": "198 226 239", // #c6e2ef
        "--color-secondary-300": "163 208 229", // #a3d0e5
        "--color-secondary-400": "94 173 210", // #5eadd2
        "--color-secondary-500": "25 138 191", // #198abf
        "--color-secondary-600": "23 124 172", // #177cac
        "--color-secondary-700": "19 104 143", // #13688f
        "--color-secondary-800": "15 83 115", // #0f5373
        "--color-secondary-900": "12 68 94", // #0c445e
        // tertiary | #62a0ea 
        "--color-tertiary-50": "231 241 252", // #e7f1fc
        "--color-tertiary-100": "224 236 251", // #e0ecfb
        "--color-tertiary-200": "216 231 250", // #d8e7fa
        "--color-tertiary-300": "192 217 247", // #c0d9f7
        "--color-tertiary-400": "145 189 240", // #91bdf0
        "--color-tertiary-500": "98 160 234", // #62a0ea
        "--color-tertiary-600": "88 144 211", // #5890d3
        "--color-tertiary-700": "74 120 176", // #4a78b0
        "--color-tertiary-800": "59 96 140", // #3b608c
        "--color-tertiary-900": "48 78 115", // #304e73
        // success | #0f6d00 
        "--color-success-50": "219 233 217", // #dbe9d9
        "--color-success-100": "207 226 204", // #cfe2cc
        "--color-success-200": "195 219 191", // #c3dbbf
        "--color-success-300": "159 197 153", // #9fc599
        "--color-success-400": "87 153 77", // #57994d
        "--color-success-500": "15 109 0", // #0f6d00
        "--color-success-600": "14 98 0", // #0e6200
        "--color-success-700": "11 82 0", // #0b5200
        "--color-success-800": "9 65 0", // #094100
        "--color-success-900": "7 53 0", // #073500
        // warning | #f6d32d 
        "--color-warning-50": "254 248 224", // #fef8e0
        "--color-warning-100": "253 246 213", // #fdf6d5
        "--color-warning-200": "253 244 203", // #fdf4cb
        "--color-warning-300": "251 237 171", // #fbedab
        "--color-warning-400": "249 224 108", // #f9e06c
        "--color-warning-500": "246 211 45", // #f6d32d
        "--color-warning-600": "221 190 41", // #ddbe29
        "--color-warning-700": "185 158 34", // #b99e22
        "--color-warning-800": "148 127 27", // #947f1b
        "--color-warning-900": "121 103 22", // #796716
        // error | #a51d2d 
        "--color-error-50": "242 221 224", // #f2dde0
        "--color-error-100": "237 210 213", // #edd2d5
        "--color-error-200": "233 199 203", // #e9c7cb
        "--color-error-300": "219 165 171", // #dba5ab
        "--color-error-400": "192 97 108", // #c0616c
        "--color-error-500": "165 29 45", // #a51d2d
        "--color-error-600": "149 26 41", // #951a29
        "--color-error-700": "124 22 34", // #7c1622
        "--color-error-800": "99 17 27", // #63111b
        "--color-error-900": "81 14 22", // #510e16
        // surface | #003a67 
        "--color-surface-50": "217 225 232", // #d9e1e8
        "--color-surface-100": "204 216 225", // #ccd8e1
        "--color-surface-200": "191 206 217", // #bfced9
        "--color-surface-300": "153 176 194", // #99b0c2
        "--color-surface-400": "77 117 149", // #4d7595
        "--color-surface-500": "0 58 103", // #003a67
        "--color-surface-600": "0 52 93", // #00345d
        "--color-surface-700": "0 44 77", // #002c4d
        "--color-surface-800": "0 35 62", // #00233e
        "--color-surface-900": "0 28 50", // #001c32

    }
}