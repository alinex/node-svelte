# Alinex Svelte Experience

This is a first example to evaluate for future use...
It includes all the framework configuration to make it easy usable for different use cases. It will be possible to create SSR, CSR,... and static sites.

## Developing

Once you've loaded the project and installed dependencies with `npm install` you can:

```bash
# start a development server
npm run dev
# or start the server and open the app in a new browser tab
npm run dev -- --open

# update to latest possible packages
npm update
# run svelte diagnostics check
npm run check

# run prettier to optimize file format
npm run format
# check for linker errors
npm run link
# run tests
npm run test # all
npm run test:unit
npm run test:integration
```

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
# build node application
npm run build
# build node docker and push to registry
npm run build-docker

# build static application
npm run export

# preview the application
npm run preview -- --open

# create documentation from markdown
npm run docs
```

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.

## Local Test

This are short scrip to test specific parts on my own host:

Static site through local Nginx:

```bash
bin/stup-static-localhost
````

## Running

Depending on how you build it you have to look at:

- [static export](src/README-static.md)
- [node application](src/README-node.md)
- [docker image](src/README-docker.md)

## Read More

A detailed documentation is available under https://alinex.gitlab.io/node-svelte.
