import { vitePreprocess } from '@sveltejs/kit/vite';
//import adapter from '@sveltejs/adapter-auto';
import node from '@sveltejs/adapter-node';
import stat from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://kit.svelte.dev/docs/integrations#preprocessors
	// for more information about preprocessors
	preprocess: [vitePreprocess({})],

	vitePlugin: {
		inspector: true,
	},
	kit: {
		adapter: process.env.STATIC
			? // generate static site
			stat({
				// default options are shown. On some platforms
				// these options are set automatically — see below
				pages: 'build',
				assets: 'build',
				fallback: undefined,
				precompress: true,
				strict: true
			})
			: // generate node server
			node({ precompress: true }),
		alias: { $i18n: 'src/i18n' },
		prerender: {
			concurrency: 1 // had to be set to 1 for for i18n
		}
	}
};

export default config;
